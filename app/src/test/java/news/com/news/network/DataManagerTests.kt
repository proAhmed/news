package news.com.news.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.reactivex.Single
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import news.com.news.MainScreenViewModel
import org.junit.Before
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner


@RunWith(MockitoJUnitRunner::class)
public class DataManagerTests {
    @Mock
    private lateinit var mockRepository: DataManager

    private val schedulerProvider = SchedulerProvider(Schedulers.trampoline(), Schedulers.trampoline())

    private lateinit var mainScreenViewModel: MainScreenViewModel

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        mainScreenViewModel = MainScreenViewModel(mockRepository)
    }

    @Test
    fun showDataFromApi() {
        Mockito.`when`(mockRepository.getHeadLineNews("")).thenReturn(Single.just(IpAddress("20.0.0.0")))

        val testObserver = TestObserver<IpAddress>()

        mainScreenViewModel.getData()
                .subscribe(testObserver)

        testObserver.assertNoErrors()
        testObserver.assertValue { ipAddress -> ipAddress.ip.equals("20.0.0.0") }
    }
    @Test
    fun getSourceNews() {
    }

    @Test
    fun getHeadLineNews() {
    }
}