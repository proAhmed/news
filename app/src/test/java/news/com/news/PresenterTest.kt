package news.com.news

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import junit.framework.TestCase.assertEquals
import news.com.news.kotlin_test.Presenter
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule

class PresenterTest {
    @get:Rule
    var rule:TestRule = InstantTaskExecutorRule()

    @Test
    fun showTitle(){
        val presenter = Presenter()
        presenter.showTitle("title")
        assertEquals("title",presenter.titleData.value)
    }
}