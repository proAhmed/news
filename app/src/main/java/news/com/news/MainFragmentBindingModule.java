package news.com.news;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import news.com.news.login.FirstPageRegister;
import news.com.news.login.SecondPageRegister;
import news.com.news.main_screen.FavoriteFragment;
import news.com.news.main_screen.MainScreen;
import news.com.news.main_screen.MyFavoriteScreen;
import news.com.news.main_screen.ProfileScreen;
import news.com.news.main_screen.SearchScreen;
import news.com.news.main_screen.SettingScreen;

@Module
public abstract class MainFragmentBindingModule {
    @ContributesAndroidInjector
    abstract MainScreen provideMainScreen();
    @ContributesAndroidInjector
    abstract FirstPageRegister provideFirstPageRegister();
    @ContributesAndroidInjector
    abstract SecondPageRegister provideSecondPageRegister();
    @ContributesAndroidInjector
    abstract FavoriteFragment provideFavoriteFragment();
    @ContributesAndroidInjector
    abstract SearchScreen provideSearchScreen();
    @ContributesAndroidInjector
    abstract MyFavoriteScreen provideMyFavoriteScreen();
    @ContributesAndroidInjector
    abstract ProfileScreen provideProfileScreen();
    @ContributesAndroidInjector
    abstract SettingScreen provideSettingScreen();
}
