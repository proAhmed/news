package news.com.news.util;

public class KeyUtil {

    public static final String API_VALUE = "a2347e0acd92471aac418865553145f7";
    public static final String API_KEY = "apiKey";
    public static final String LANGUAGE_KEY = "language";
    public static final String LANGUAGE_AR = "ar";
    public static final String CATEGORY_KEY = "category";
    public static final String COUNTRY_KEY = "country";
    public static final String TOP_HEADLINES = "top-headlines";
    public static final String SOURCE = "sources";
    public static final String EVERY_THING = "everything";
 //   public static final String BASE_URL = "https://newsapi.org/v2/";
    public static final String BASE_URL = "http://18.223.31.28:8080/api/";
    public static final String FROM_DATE = "from";
    public static final String TO_DATE = "to";
    public static final String ABOUT_KEY = "q";
    public static final String SORT_BY = "sortBy";
    public static final String PUBLISHED_BY = "domains";
    public static final String SEARCH_DATE_FORMAT = "yyyy-mm-dd";
    public static final String ARTICLE = "article";
    public static final String COUNTRY = "eg";
    public static final String NEWS_FEEDS = "nw-feeds";
    public static final String REGISTER_USER = "register-with-favorites";
    public static final String LOGIN_USER = "login";
    public static final String USER_NAME = "userName";
    public static final String REGISTER_TOPIC = "register-topic";
    public static final String FEEDS_BY_USER = "nw-feeds-by-user-topics/";
    public static final String FEEDS_BY_TOPIC = "nw-feeds-by-topic/";
    public static final String KEY_TOPIC = "nw-topic";
    public static final String TOPIC = "topic";
    public static final String RATING = "save-nw-feed-rate";
   public static final String CATEGORY_BUSINESS = "business";
    public static final String CATEGORY_HEALTH = "health";
    public static final String CATEGORY_SCIENCE = "science";
    public static final String CATEGORY_TECHNOLOGY = "technology";
    public static final String CATEGORY_SPORTS = "sports";




}
