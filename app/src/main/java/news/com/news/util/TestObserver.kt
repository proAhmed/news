package news.com.news.util

import io.reactivex.observers.TestObserver

fun<T> LiveData<T>.test():TestObserver<T>{
    return TestObserver.test(this)
}
