package news.com.news.util;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.Timer;
import java.util.TimerTask;

import androidx.annotation.Nullable;

public class ServiceCount extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        countDownStart();

    }

    public void sendData(int value) {
        Intent intent = new Intent("YourAction");
        Bundle bundle = new Bundle();
        bundle.putInt("time", value); // put extras you want to pass with broadcast. This is optional
        bundle.putString("valueName", "The value you want in the activity");
        intent.putExtras(bundle);
        sendBroadcast(intent);
    }

    public void countRate() {
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                new Runnable() {
                    @Override
                    public void run() {
                        //   do your stuff here.
                        int counter = 0;
                        ++counter;
                        Log.d("ssssppp111", "" + counter);
                    }
                };
            }
        }, 1000, 1000);

        Thread t = new Thread() {

            @Override
            public void run() {
                try {
                    while (!isInterrupted()) {
                        Thread.sleep(1000);
                        new Runnable() {
                            @Override
                            public void run() {
                                int counter = 0;
                                ++counter;
                                Log.d("ssssppp222", "" + counter);
                                //  sendData(counter);
                            }
                        };
                    }
                } catch (InterruptedException e) {
                }
            }
        };

        t.start();
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int counter = 0;
                ++counter;
                handler.postDelayed(this, 1000L);
                //  if(counter[0]>30){
                ++counter;
                Log.d("ssssppp333", "" + counter);
                //  }
                handler.removeCallbacks(this);
            }
        }, 1000L);
    }

    static int sta;

    public void countDownStart(

    ) {
        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                handler.postDelayed(this, 1000);
                try {
                    ++sta;
                    if (sta == 30) {
                        sendData(sta);
                    }


                } catch (Exception e) {
                    Log.d("pppp", e.toString());
                }
            }
        };
        handler.postDelayed(runnable, 1);
    }
}
