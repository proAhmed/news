package news.com.news.util;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateUtil {


    public static String convertDate(String inputDate){

        Date parsed;
        String outputDate = "";
        Locale locale = new Locale("ar");
        String inputFormat = "yyyy-MM-dd'T'hh:mm:ss'Z'";
        String outputFormat = "dd-MMM-yyyy'|'hh:mm";
        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, locale);

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (Exception e) {
            Log.d("",e.toString()+ "  - dateFormat");
        }

        return outputDate;

    }
}
