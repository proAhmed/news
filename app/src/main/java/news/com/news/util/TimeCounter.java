package news.com.news.util;

import android.os.Handler;

public class TimeCounter {


    public static int countRate() {
        Handler handler = new Handler();
        final int[] counter = {0};
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (++counter[0] < 25) {
                    handler.postDelayed(this, 1000L);
                    return;
                }
                handler.removeCallbacks(this);
            }
        }, 1000L);
        return counter[0];
    }

    public static int returnRating(int count, int actions) {

        if(actions==1&&count>30){
            return 16;
        }else
        if (actions == 1 && count > 4) {
            return 8;
        }else
        if (count > 4) {
            return 4;
        }else {
            return 0;
        }
    }
}
