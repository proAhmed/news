package news.com.news.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import javax.inject.Inject;

import news.com.news.R;
import news.com.news.model.FavoriteModel;



public class FavoriteData {
    private  SharedPreferences mPrefs;
    @Inject
    Context context;

    @Inject
    public FavoriteData() {
    }

    public final ArrayList<FavoriteModel> buildArray(){
       ArrayList<FavoriteModel> favoriteModels = new ArrayList<>();
       favoriteModels.add(new FavoriteModel("سوق المال","business",1, R.drawable.business_icon));
       favoriteModels.add(new FavoriteModel("صحة","health",2,R.drawable.health_icon));
       favoriteModels.add(new FavoriteModel("علوم","science",3,R.drawable.science_icon));
       favoriteModels.add(new FavoriteModel("تقنية","technology",5,R.drawable.technology_icon));
       favoriteModels.add(new FavoriteModel("رياضة","sports",4,R.drawable.sport_icons));
       return favoriteModels;
   }
    public final ArrayList<FavoriteModel> buildArrays(){
        ArrayList<FavoriteModel> favoriteModels = new ArrayList<>();
        favoriteModels.add(new FavoriteModel("الرئيسية","All",0, R.drawable.home_icon));
        favoriteModels.add(new FavoriteModel("سوق المال","business",1, R.drawable.business_icon));
        favoriteModels.add(new FavoriteModel("صحة","health",2,R.drawable.health_icon));
        favoriteModels.add(new FavoriteModel("علوم","science",3,R.drawable.science_icon));
        favoriteModels.add(new FavoriteModel("تقنية","technology",5,R.drawable.technology_icon));
        favoriteModels.add(new FavoriteModel("رياضة","sports",4,R.drawable.sport_icons));
        return favoriteModels;
    }
    public String getKey(String key) {
        return mPrefs.getString(key, "");
    }

    public void setKey(String key,String value) {
        SharedPreferences.Editor mEditor = mPrefs.edit();
        mEditor.putString(key, value);
        mEditor.apply();
    }
   public  void sharedBuild(Context context){
       mPrefs = PreferenceManager.getDefaultSharedPreferences(context);
   }

   public ArrayList<FavoriteModel> filteredAddMyFavorite(ArrayList<FavoriteModel> arrayList){
       ArrayList<FavoriteModel> favoriteModels = new ArrayList<>();
       favoriteModels = arrayList;
       for(int i=0;i<favoriteModels.size();i++){
           if(favoriteModels.get(i).getAddToUser()==1){
               arrayList.remove(favoriteModels.get(i));
           }
       }
       return favoriteModels;
   }
    public ArrayList<FavoriteModel> filteredMyFavorite(ArrayList<FavoriteModel> arrayList){
        ArrayList<FavoriteModel> favoriteModels = new ArrayList<>();
        for(int i=0;i<arrayList.size();i++){
            if(arrayList.get(i).getAddToUser()==1){
                favoriteModels.add(arrayList.get(i));
            }
        }
        return favoriteModels;
    }
    public void saveArrayList(Context context,ArrayList<FavoriteModel> list, String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        editor.putString(key, json);
        editor.apply();     // This line is IMPORTANT !!!
    }

    public void saveEmail(Context context,String email){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("email", email);
        editor.apply();     // This line is IMPORTANT !!!
    }
    public void saveName(Context context,String email){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("name", email);
        editor.apply();     // This line is IMPORTANT !!!
    }
    public void saveUserName(Context context,String userName){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("userName", userName);
        editor.apply();     // This line is IMPORTANT !!!
    }
    public ArrayList<FavoriteModel> getArrayList(Context context,String key){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Gson gson = new Gson();
        String json = prefs.getString(key, null);
        Type type = new TypeToken<ArrayList<FavoriteModel>>() {}.getType();
        return gson.fromJson(json, type);
    }

    public String getEmail(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("email", "");
    }

    public String getName(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("name", "");
    }
    public String getUserName(Context context){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("userName", "");
    }
}
