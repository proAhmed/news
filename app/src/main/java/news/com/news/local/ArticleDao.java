package news.com.news.local;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Single;
import news.com.news.model.Article;

@Dao
public interface ArticleDao {
    @Query("SELECT * FROM  article WHERE id = :id")
    Single<Article> getSelectedArticle(String id);

    @Query("SELECT * FROM  article ")
    Single<List<Article>> getArticleList();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Article article);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Article> articleList);

    @Delete
    void delete(Article article);

    @Update
    void update(Article article);
}
