package news.com.news.local;


import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import news.com.news.model.Article;
import news.com.news.model.FavoriteModel;
import news.com.news.model.NewsModel;
import news.com.news.network.ApiInterface;
import news.com.news.network.DataManager;

public class NewsRepository {
    FavoriteDao favoriteDao;
    DataManager dataManager;

    @Inject
    public NewsRepository(FavoriteDao favoriteDao, DataManager dataManager) {
        this.favoriteDao = favoriteDao;
        this.dataManager = dataManager;
    }

    Single<NewsModel> loadFavorite() {
        getDataFromServer();
        favoriteDao.getSelectedFavorites();
        return getDataFromServer();
    }

    private Single<NewsModel> getDataFromServer() {
        Single<NewsModel> observable1 = dataManager.getHeadLineNews("business");
        Single<NewsModel> observable2 = dataManager.getHeadLineNews("health");
        Single<NewsModel> observable3 = dataManager.getHeadLineNews("science");
        Single<NewsModel> observable4 = dataManager.getHeadLineNews("technology");
        Single<NewsModel> observable5 = dataManager.getHeadLineNews("sports");

        return Single.zip(observable1.subscribeOn(Schedulers.io()),
                observable2.subscribeOn(Schedulers
                        .io()), observable3.subscribeOn(Schedulers.io()),
                observable4.subscribeOn(Schedulers.io()),
                observable5.subscribeOn(Schedulers.io()), (type1, type2, type3, type4, type5) -> {

                    List<Article> list = new ArrayList<>();
                    for (int i = 0; i < type1.getArticles().size(); i++) {
                        type1.getArticles().get(i).setKeyWord("سوق المال");
                    }
                    for (int i = 0; i < type2.getArticles().size(); i++) {
                        type2.getArticles().get(i).setKeyWord("صحة");
                    }
                    for (int i = 0; i < type3.getArticles().size(); i++) {
                        type3.getArticles().get(i).setKeyWord("علوم");
                    }
                    for (int i = 0; i < type4.getArticles().size(); i++) {
                        type4.getArticles().get(i).setKeyWord("تقنية");
                    }
                    for (int i = 0; i < type5.getArticles().size(); i++) {
                        type5.getArticles().get(i).setKeyWord("رياضة");
                    }
                    list.addAll(type1.getArticles());
                    list.addAll(type2.getArticles());
                    list.addAll(type3.getArticles());
                    list.addAll(type4.getArticles());
                    list.addAll(type5.getArticles());
                    NewsModel newsModel = new NewsModel();
                    newsModel.setArticles(list);
                    return newsModel;
                });
    }
}
