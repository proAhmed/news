package news.com.news.local;


import androidx.room.Database;
import androidx.room.RoomDatabase;
import news.com.news.model.Article;
import news.com.news.model.FavoriteModel;

@Database(entities = {FavoriteModel.class,Article.class}, version = 2, exportSchema = false)
abstract public class NewsDb extends RoomDatabase {
    public abstract FavoriteDao favoriteDao();
    public abstract ArticleDao articleDao();

}
