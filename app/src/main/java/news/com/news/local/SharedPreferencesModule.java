package news.com.news.local;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Inject;

import dagger.Module;
import dagger.Provides;
import news.com.news.MyApplicationScope;

@Module
public class SharedPreferencesModule {

    @MyApplicationScope
    @Provides
    @Inject
    SharedPreferences provideSharedPreferences(Context context) {
        return context.getSharedPreferences("PrefName",Context.MODE_PRIVATE);
    }
}
