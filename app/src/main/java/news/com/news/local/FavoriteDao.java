package news.com.news.local;

import java.util.ArrayList;
import java.util.List;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import io.reactivex.Observable;
import io.reactivex.Single;
import news.com.news.model.FavoriteModel;

@Dao
public interface FavoriteDao {
    @Query("SELECT * FROM  favorite ")
    Observable<FavoriteModel> getFavorites();

    @Query("SELECT * FROM  favorite ")
    Single<List<FavoriteModel>> getSelectedFavorites();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(FavoriteModel favoriteModel);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(ArrayList<FavoriteModel> favoriteModels);

    @Delete
    int delete(FavoriteModel favoriteModel);

    @Update
    void update(FavoriteModel favoriteModel);
}
