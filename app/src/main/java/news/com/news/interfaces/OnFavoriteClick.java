package news.com.news.interfaces;

import news.com.news.model.FavoriteModel;

public interface OnFavoriteClick {
    void onAllClick(FavoriteModel favoriteModel,int pos);
}
