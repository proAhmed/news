package news.com.news.interfaces;

import news.com.news.model.Article;

public interface OnAllClickPos {
    void onAllClick(Article article,int adapterPosition);
    void onAddArticleToFavorite(Article article,int adapterPosition);

}
