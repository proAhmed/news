package news.com.news.interfaces;

import news.com.news.model.FavoriteModel;
import news.com.news.model.TopicModel;

public interface OnTopicClick {
    void onAllClick(TopicModel favoriteModel, int pos);
}
