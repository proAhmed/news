package news.com.news;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import news.com.news.main_screen.FavoriteFragment;

public class MainActivity extends BaseActivity {

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if(savedInstanceState == null) {
//
//            getSupportFragmentManager().beginTransaction().add(R.id.container, new FavoriteFragment()).commitAllowingStateLoss();
//        }
    }
}
