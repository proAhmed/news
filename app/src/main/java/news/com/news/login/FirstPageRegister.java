package news.com.news.login;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import news.com.news.BaseFragment;
import news.com.news.DataBuild;
import news.com.news.R;
import news.com.news.model.User;


public class FirstPageRegister extends BaseFragment {
    @BindView(R.id.spAge)
    Spinner spAge;
    @BindView(R.id.chEmployee)
    CheckBox chEmployee;
    @BindView(R.id.chGeneralWork)
    CheckBox chGeneralWork;
    @BindView(R.id.rdWork)
    RadioGroup rdWork;
    @BindView(R.id.rdGeneral)
    RadioButton rdGeneral;
    @BindView(R.id.rdPrivate)
    RadioButton rdPrivate;
    @BindView(R.id.spTax)
    Spinner spTax;
    @BindView(R.id.spLearning)
    Spinner spLearning;
    @BindView(R.id.rdGender)
    RadioGroup rdGender;
    @BindView(R.id.rdMale)
    RadioButton rdMale;
    @BindView(R.id.rdFemale)
    RadioButton rdFemale;
    @BindView(R.id.chDependent)
    CheckBox chDependent;
    @BindView(R.id.edNum)
    EditText edNum;
    @BindView(R.id.btnNext)
    Button btnNext;
    private String age = "";
    private String tax = "";
    private String learning = "";
    private String gender = "";
    private String workType = "";
    private boolean generalWork;
    private boolean employee;
    private int dependentNumber = 0;
    private boolean isChosen,isCheck;


    @Override
    protected int layoutRes() {
        return R.layout.first_register_page;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            initView();
        }
    }

    private void initView() {
        TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
        toolbar_title.setText("تسجيل مستخدم جديد");
        edNum.setEnabled(false);
        ArrayList<String> ageList = DataBuild.buildAgeList();
        ArrayAdapter<String> ageAdapter = new ArrayAdapter<>(getBaseActivity(),
                android.R.layout.simple_spinner_item, ageList);
        ageAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAge.setAdapter(ageAdapter);
        spAge.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                age = ageList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> taxList = DataBuild.buildTaxList();
        ArrayAdapter<String> taxAdapter = new ArrayAdapter<>(getBaseActivity(),
                android.R.layout.simple_spinner_item, taxList);
        taxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTax.setAdapter(taxAdapter);
        spTax.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                tax = taxList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayList<String> learningList = DataBuild.buildLearnList();
        ArrayAdapter<String> learningAdapter = new ArrayAdapter<>(getBaseActivity(),
                android.R.layout.simple_spinner_item, learningList);
        taxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLearning.setAdapter(learningAdapter);
        spLearning.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                learning = learningList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        rdWork.setOnCheckedChangeListener((group, checkedId) -> {
            // find which radio button is selected
            if (checkedId == R.id.rdGeneral) {
                workType = "general";
            } else if (checkedId == R.id.rdPrivate) {
                workType = "private";
            }
        });

        rdGender.setOnCheckedChangeListener((group, checkedId) -> {
            // find which radio button is selected
            if (checkedId == R.id.rdMale) {
                gender = "male";
            } else if (checkedId == R.id.rdFemale) {
                gender = "female";
            }
        });
        chGeneralWork.setOnCheckedChangeListener((buttonView, isChecked) -> generalWork = isChecked);
        chEmployee.setOnCheckedChangeListener((buttonView, isChecked) -> employee = isChecked);
        chDependent.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                edNum.setEnabled(true);
            } else {
                edNum.setEnabled(false);
            }
        });
        btnNext.setOnClickListener(v -> {
            validate();
            if (isChosen&&isCheck) {
                User user = new User();
                user.setAge(age);
                user.setGender(gender);
                user.setDependency(dependentNumber+"");
                user.setEducation(learning);
                user.setTaxes(tax);
                user.setWorkType(workType);
                Fragment secondPageRegister = new SecondPageRegister();
                Bundle bundle = new Bundle();
                bundle.putParcelable("user", user);
                secondPageRegister.setArguments(bundle);
                getBaseActivity().getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, secondPageRegister).addToBackStack("")
                        .commit();
            }else {
                Toast.makeText(getBaseActivity(),"من فضلك ادخل البيانات كاملة",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void validate() {
        if(!age.isEmpty()&&!tax.isEmpty()&&!learning.isEmpty()
                &&!workType.isEmpty()&&!gender.isEmpty()
                &&generalWork || employee){
            isChosen = true;
        }

        if (chDependent.isChecked() && !edNum.getText().toString().isEmpty()) {
            isCheck = true;
        } else if (!chDependent.isChecked()) {
            isCheck = true;
        }
    }
}
