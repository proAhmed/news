package news.com.news.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

import javax.inject.Inject;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import news.com.news.BaseActivity;
import news.com.news.MainScreenViewModel;
import news.com.news.R;
import news.com.news.ViewModelFactory;

public class ResetPasswordActivity extends BaseActivity {
    @Inject
    ViewModelFactory viewModelFactory;
    private MainScreenViewModel viewModel;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.btnReset)
    Button btnReset;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @Override
    protected int layoutRes() {
        return R.layout.activity_reset_password;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        toolbar_title.setText("إعادة تعيين كلمة المرور");
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(MainScreenViewModel.class);
        btnReset.setOnClickListener(v -> {


                String email = edEmail.getText().toString();
                resetPassword(email);
        });



   //     observableViewModel();
    }

    private void resetPassword(String emailAddress){
        FirebaseAuth auth = FirebaseAuth.getInstance();

        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        Intent intent = new Intent(ResetPasswordActivity.this,LoginActivity.class);
                        startActivity(intent);
                        Toast.makeText(ResetPasswordActivity.this, "Check email to reset your password!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ResetPasswordActivity.this, "Fail to send reset password email!", Toast.LENGTH_SHORT).show();
                    }

                });
    }

    private void observableViewModel(){
        viewModel.getRepos().getValue();
        viewModel.getRepos().observe(this,newModel->{
            if (newModel!=null){
                Log.d("kkk",newModel.size()+"  kkk");
            }

        });
        viewModel.getError().observe(this, error -> {
            if(error!=null){
                if(error){
                    Log.d("kkk",error+"");

                }
            }
        });
        viewModel.getLoading().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean isLoading) {

            }
        });
    }

}
