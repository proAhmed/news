package news.com.news.login;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import butterknife.BindView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import news.com.news.BaseFragment;
import news.com.news.DataBuild;
import news.com.news.MainActivity;
import news.com.news.NavigateActivity;
import news.com.news.R;
import news.com.news.model.Contact;
import news.com.news.model.User;
import news.com.news.model.UserResponse;
import news.com.news.network.DataManager;
import news.com.news.util.FavoriteData;


public class SecondPageRegister extends BaseFragment {

    @BindView(R.id.edUserName)
    EditText edUserName;
    @BindView(R.id.edName)
    EditText edName;
    @BindView(R.id.edJob)
    EditText edJob;
    @BindView(R.id.spReligion)
    Spinner spReligion;
    @BindView(R.id.edMobile)
    EditText edMobile;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.edPass)
    EditText edPass;
    @BindView(R.id.edRePass)
    EditText edRePass;
    @BindView(R.id.spGovernate)
    Spinner spGovernate;
    @BindView(R.id.spCity)
    Spinner spCity;
    @BindView(R.id.edDistrict)
    EditText edDistrict;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    private String name ="",userName ="";
    private String job ="";
    private String religion ="";
    private String mobile ="";
    private String email = "";
    private String governate ="";
    private String city ="";
    private String district;
    private String pass;
    private User user;
    private boolean isChosen;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    private static boolean mobileExist;
    private FavoriteData favoriteData;
    private CompositeDisposable disposable;
    @Inject
    DataManager dataManager;
    @Override
    protected int layoutRes() {
        return R.layout.second_register_page;
    }
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        if (getActivity() != null) {
            initView();
        }
    }


    private void initView(){
        TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
        toolbar_title.setText("تسجيل مستخدم جديد");
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Bundle bundle = getArguments();
        favoriteData = new FavoriteData();
        disposable = new CompositeDisposable();
        if(bundle!=null){
            user = bundle.getParcelable("user");
        }
        edMobile.setOnFocusChangeListener((v, hasFocus) -> {
            if(edMobile.getText().length()>10&&!hasFocus){
               checkMobile(edMobile.getText().toString());

            }
        });
       if(getActivity()!=null) {
           ArrayList<String> religionList = DataBuild.buildReligionList();
           ArrayAdapter<String> religionAdapter = new ArrayAdapter<>(getActivity(),
                   android.R.layout.simple_spinner_item, religionList);
           religionAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           spReligion.setAdapter(religionAdapter);
           spReligion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                   religion = religionList.get(position);
               }

               @Override
               public void onNothingSelected(AdapterView<?> parent) {

               }
           });

           ArrayList<String> governateList = DataBuild.buildGovernateList();
           ArrayAdapter<String> governateAdapter = new ArrayAdapter<>(getActivity(),
                   android.R.layout.simple_spinner_item, governateList);
           governateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           spGovernate.setAdapter(governateAdapter);
           spGovernate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                   governate = governateList.get(position);
               }

               @Override
               public void onNothingSelected(AdapterView<?> parent) {

               }
           });

           ArrayList<String> cityList = DataBuild.buildCityList();
           ArrayAdapter<String> taxAdapter = new ArrayAdapter<>(getActivity(),
                   android.R.layout.simple_spinner_item, cityList);
           taxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
           spCity.setAdapter(taxAdapter);
           spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                   city = cityList.get(position);
               }

               @Override
               public void onNothingSelected(AdapterView<?> parent) {

               }
           });
       }

        btnRegister.setOnClickListener(v -> {
            validate();
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("username",user.getUsername());
            jsonObject.addProperty("email",user.getEmail());
            jsonObject.addProperty("name",user.getName());
            jsonObject.addProperty("age",user.getAge());
            jsonObject.addProperty("city",user.getCity());
            jsonObject.addProperty("password",user.getPassword());
            jsonObject.addProperty("area",user.getArea());
            jsonObject.addProperty("gender",user.getGender());
            jsonObject.addProperty("phoneNumber",user.getPhoneNumber());
            jsonObject.addProperty("positionTitle",user.getPositionTitle());
            jsonObject.addProperty("education",user.getEducation());
            jsonObject.addProperty("workingStatus",true);
            jsonObject.addProperty("accupation","private");
            jsonObject.addProperty("section",user.getSection());
            jsonObject.addProperty("taxes",user.getTaxes());
            jsonObject.addProperty("dependency",user.getDependency());
            Single<UserResponse> observable = dataManager.addUser(jsonObject);
            disposable.add(observable.subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<UserResponse>() {
                        @Override
                        public void onSuccess(UserResponse value) {
                            if(value!=null){
                                favoriteData.saveName(getActivity(),name);
                                favoriteData.saveEmail(getActivity(),email);
                                Intent intent = new Intent(getActivity(),NavigateActivity.class);
                                startActivity(intent);
                                Log.d("pppp",value.toString());
                            }
                           }

                        @Override
                        public void onError(Throwable e) {
                            Log.d("pppp",e.toString());

                        }
                    }));
            if(isChosen&&user!=null){
                user.setArea(district);
                user.setName(name);
                user.setUsername(userName);
                user.setPassword(pass);
                user.setCity(city);
                user.setCity(governate);
                user.setEmail(email);
                user.setPhoneNumber(mobile);
//                user.setReligion(religion);
                user.setWorkType(job);
                if(!mobileExist){
               // registerUser(email,pass);
                }else {
                    Toast.makeText(getActivity(),"من فضلك ادخل رقم هاتف مختلف",Toast.LENGTH_LONG).show();

                }
            }else {
                Toast.makeText(getActivity(),"من فضلك ادخل البيانات كاملة",Toast.LENGTH_LONG).show();
            }
        });
    }
    private void validate(){

        district = edDistrict.getText().toString();
        email = edEmail.getText().toString();
        mobile = edMobile.getText().toString();
        name = edName.getText().toString();
        userName = edUserName.getText().toString();
        pass = edPass.getText().toString();
        String rePass = edRePass.getText().toString();
        job = edJob.getText().toString();

        if(!district.isEmpty()&&!email.isEmpty()&&!mobile.isEmpty()&&!userName.isEmpty()
                &&!governate.isEmpty()&&!name.isEmpty()
                &&city.isEmpty()&&!religion.isEmpty()){
            isChosen = true;
        }

        if(!pass.isEmpty()&&pass.equals(rePass)){
            isChosen = true;
        }
    }

    private void registerUser(String email,String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(Objects.requireNonNull(getActivity()), task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("ooo", "createUserWithEmail:success");
                        mAuth.getCurrentUser();
                        FirebaseUser user =  Objects.requireNonNull(task.getResult()).getUser();
                        createUser(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("createUserWithEmail", task.getException()+"");
                        Toast.makeText(getActivity(), "Authentication failed.",
                                Toast.LENGTH_SHORT).show();

                    }

                    // ...
                });
    }


    private void createUser(FirebaseUser firebaseUser) {
        Contact contact = new Contact();
        contact.setMobile(mobile);
        contact.setName(name);
        mDatabase.child("user").child(firebaseUser.getUid()).setValue(user).
                addOnCompleteListener(task -> {
                    if (task.isSuccessful()) {
                        mDatabase.child("contact").push().setValue(contact).addOnCompleteListener(task1 -> {
                            favoriteData.saveEmail(getBaseActivity(),email);
                            favoriteData.saveName(getBaseActivity(),name);
                            favoriteData.saveUserName(getBaseActivity(),userName);

                            Log.d("createUserWithEmail", task1.getException()+"");
                            Intent intent = new Intent(getActivity(),NavigateActivity.class);
                            startActivity(intent);
                        });
                    }
                });
    }

    private void checkMobile(String mobile){

        Query query = mDatabase.child("contact").orderByChild("mobile");

        query.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    String text = dataSnapshot.getValue().toString();
                    if (text.contains(mobile+",")) {
                        mobileExist = true;
                        edMobile.setError("mobile in used");
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.d("oooo",databaseError.getMessage());
            }
        });
    }
}
