package news.com.news.login;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import news.com.news.BaseActivity;
import news.com.news.MainActivity;
import news.com.news.NavigateActivity;
import news.com.news.R;
import news.com.news.User;

public class RegisterActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @Override
    protected int layoutRes() {
        return R.layout.activity_register;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new FirstPageRegister())
                .commit();
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

       // registerUser("geo.aaaar@gmail.com","1234567");
//      String uuid=  Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
//        updateData(uuid);

        FirebaseUser user = mAuth.getCurrentUser();
        if(user!=null) {
            DatabaseReference mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
            Query query = mFirebaseDatabaseReference.child(user.getUid()).orderByChild("mobile").equalTo("01090207200");

            ValueEventListener valueEventListener = new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Log.d("ooo", "createUserWithEmail:success");
                    Intent intent = new Intent(RegisterActivity.this, NavigateActivity.class);
                    startActivity(intent);


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.d("ooo", "createUserWithEmail:success");

                }
            };
            query.addValueEventListener(valueEventListener);
        }
    }

    private void registerUser(String email,String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d("ooo", "createUserWithEmail:success");
                         mAuth.getCurrentUser();
                        FirebaseUser user =  Objects.requireNonNull(task.getResult()).getUser();
                        createUser(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.d("createUserWithEmail", task.getException()+"");
                        Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                Toast.LENGTH_SHORT).show();

                    }

                    // ...
                });
    }

    private void updateData(String firebaseUser){
        mDatabase.child("user").child(firebaseUser).child("education").setValue("master");

    }

    private void createUser(FirebaseUser firebaseUser) {
        User user = new User();
        user.setAge(25);
        user.setEducation("");
        user.setUserName("ahmed");
        user.setEmail(firebaseUser.getEmail());
        user.setMobile("0103020202");
        user.setPassword("1234567");

        mDatabase.child("user").child(firebaseUser.getUid()).setValue(user).
                addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d("createUserWithEmail", task.getException()+"");

                        }

                    }
                });
    }

}
