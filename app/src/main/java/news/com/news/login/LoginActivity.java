package news.com.news.login;


import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.JsonObject;

import javax.inject.Inject;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import news.com.news.BaseActivity;
import news.com.news.MainActivity;
import news.com.news.NavigateActivity;
import news.com.news.R;
import news.com.news.model.NewsModel;
import news.com.news.model.UserResponse;
import news.com.news.network.DataManager;
import news.com.news.util.FavoriteData;

public class LoginActivity extends BaseActivity {
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.tvForgetPass)
    TextView tvForgetPass;

    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.edPass)
    EditText edPass;
    @BindView(R.id.btnLogin)
    Button btnLogin;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbar_title;
    @Inject
    DataManager dataManager;

    private boolean isChosen;
    private String email="";
    private String pass="";
    private FavoriteData favoriteData;
    private CompositeDisposable disposable;
    @Override
    protected int layoutRes() {
        return R.layout.activity_login;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        favoriteData = new FavoriteData();
        disposable = new CompositeDisposable();
        if(!favoriteData.getEmail(this).isEmpty()) {
            Intent intent = new Intent(LoginActivity.this, NavigateActivity.class);
            startActivity(intent);
            finish();
        }
//        mAuth = FirebaseAuth.getInstance();
//        mDatabase = FirebaseDatabase.getInstance().getReference();
        toolbar_title.setText("تسجيل دخول");
        init();


    }
    private void init(){
        btnRegister.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
            startActivity(intent);
        });

        btnLogin.setOnClickListener(v -> {
            validate();
            if(isChosen){

                login(email,pass);
            }
        });
        tvForgetPass.setOnClickListener(v -> {
            Intent intent = new Intent(LoginActivity.this,ResetPasswordActivity.class);
            startActivity(intent);
        });
    }

    private void validate(){
      pass = edPass.getText().toString();
        email = edEmail.getText().toString();
        if(!email.isEmpty()&&!pass.isEmpty()){
            isChosen=true;
        }
    }


    private void login(String email,String pass){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("username",email);
        jsonObject.addProperty("password",pass);
        Single<UserResponse> observable = dataManager.login(jsonObject);
        disposable.add(observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<UserResponse>() {
                    @Override
                    public void onSuccess(UserResponse value) {
                        if(value!=null){
                            Log.d("pppp",value.toString());
                            favoriteData.saveName(LoginActivity.this,email);
                            favoriteData.saveEmail(LoginActivity.this,email);
                            Intent intent = new Intent(LoginActivity.this,NavigateActivity.class);
                            startActivity(intent);
                            finish();
                        }


                        //    articleDao.insertAll(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("pppp",e.toString());

                    }
                }));

//                mAuth.signInWithEmailAndPassword(email, pass)
//                .addOnCompleteListener(this, task -> {
//                    if (task.isSuccessful()) {
//                       if(task.getResult()!=null&&task.getResult().getUser()!=null){
//                           task.getResult().getUser().getDisplayName();
//                           favoriteData.saveName(LoginActivity.this,email);
//                       }
//                        favoriteData.saveEmail(LoginActivity.this,email);
//                            Intent intent = new Intent(LoginActivity.this,NavigateActivity.class);
//                            startActivity(intent);
//                            finish();
////                        });
//                        // Sign in success, update UI with the signed-in user's information
//
//                    } else {
//                        // If sign in fails, display a message to the user.
//                        Toast.makeText(LoginActivity.this, "Authentication failed.",
//                                Toast.LENGTH_SHORT).show();
//                    }
//
//                    // ...
//                });
    }
}
