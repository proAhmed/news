package news.com.news.di;


import dagger.Module;
import news.com.news.ViewModelModule;

@Module(includes = ViewModelModule.class)
public class ApplicationModule {
}
