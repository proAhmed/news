package news.com.news.di;



import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import news.com.news.MainActivity;
import news.com.news.MainFragmentBindingModule;
import news.com.news.NavigateActivity;
import news.com.news.login.LoginActivity;
import news.com.news.login.RegisterActivity;
import news.com.news.login.ResetPasswordActivity;
import news.com.news.main_screen.NewsDetails;

@Module
    public abstract class ActivityBindingModule {

    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract MainActivity bindMainActivity();
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract LoginActivity bindLoginActivity();
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract ResetPasswordActivity bindResetPasswordActivity();
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract RegisterActivity bindRegisterActivity();
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract NewsDetails bindNewsDetails();
    @ContributesAndroidInjector(modules = {MainFragmentBindingModule.class})
    abstract NavigateActivity bindNavigateActivity();

}
