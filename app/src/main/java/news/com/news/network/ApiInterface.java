package news.com.news.network;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Single;
import news.com.news.model.Article;
import news.com.news.model.FavoriteModel;
import news.com.news.model.NewsModel;
import news.com.news.model.RatingResponse;
import news.com.news.model.TopicModel;
import news.com.news.model.UserResponse;
import news.com.news.util.KeyUtil;
import news.com.news.model.RegisterTopicResponse;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {
    @GET(KeyUtil.SOURCE)
    Observable<NewsModel> getSubCategory(@Query(KeyUtil.COUNTRY_KEY) String country
            ,@Query(KeyUtil.CATEGORY_KEY) String category,@Query(KeyUtil.API_KEY) String keyAPi);

    @GET(KeyUtil.TOP_HEADLINES)
    Single<NewsModel> getHeadLine(@Query(KeyUtil.COUNTRY_KEY) String country
            , @Query(KeyUtil.CATEGORY_KEY) String category, @Query(KeyUtil.API_KEY) String keyAPi);
    @GET(KeyUtil.TOP_HEADLINES)
    Observable<NewsModel> getHeadLines(@Query(KeyUtil.COUNTRY_KEY) String country
            , @Query(KeyUtil.CATEGORY_KEY) String category, @Query(KeyUtil.API_KEY) String keyAPi);
    @GET(KeyUtil.EVERY_THING)
    Single<NewsModel> getEveryThing(@Query(KeyUtil.ABOUT_KEY) String aboutKey,@Query(KeyUtil.LANGUAGE_KEY) String language
            ,@Query(KeyUtil.API_KEY) String keyAPi);

    @GET(KeyUtil.EVERY_THING)
    Observable<NewsModel> getEveryThingPublishedBy(@Query(KeyUtil.PUBLISHED_BY) String publishedBy
            ,@Query(KeyUtil.API_KEY) String keyAPi);
    @GET(KeyUtil.EVERY_THING)
    Observable<NewsModel> getEveryThingFromTo(@Query(KeyUtil.ABOUT_KEY) String publishedBy
            ,@Query(KeyUtil.FROM_DATE) String from
            ,@Query(KeyUtil.TO_DATE) String to,@Query(KeyUtil.API_KEY) String keyAPi);
    @GET(KeyUtil.NEWS_FEEDS)
    Single<List<Article>> getNews();
     @POST(KeyUtil.LOGIN_USER)
    Single<UserResponse> login(@Header("Content-Type")String header,@Body JsonObject user);
    @POST(KeyUtil.REGISTER_USER)
    Single<UserResponse> addUser(@Header("Content-Type")String header,@Body JsonObject user);
    @POST(KeyUtil.REGISTER_TOPIC)
    Single<RegisterTopicResponse> addTopicToUser(@HeaderMap Map<String, String> headers,@Body JsonObject user);
    @GET(KeyUtil.FEEDS_BY_USER+KeyUtil.USER_NAME)
    Single<List<Article>> getFavoriteNews(@Path(KeyUtil.USER_NAME) String userName);
    @GET(KeyUtil.FEEDS_BY_TOPIC+KeyUtil.TOPIC)
    Single<List<Article>> getNewsByTopic(@Path(KeyUtil.TOPIC) int topic);
    @POST(KeyUtil.RATING+"/"+KeyUtil.USER_NAME)
    Observable<RatingResponse> saveRating(@Path(KeyUtil.USER_NAME) String userName, @Body JsonObject rating);
    @GET(KeyUtil.KEY_TOPIC)
    Single<List<TopicModel>> getTopics();
}
