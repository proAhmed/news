package news.com.news.network;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Protocol;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class OfflineMockInterceptor implements Interceptor {

    private static final MediaType MEDIA_JSON = MediaType.parse("application/json");

    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {

        return new Response.Builder()
                .body(ResponseBody.create(MEDIA_JSON, "\"hello\""))
                .request(chain.request())
                .protocol(Protocol.HTTP_2)
                .code(200)
                .build();

    }
}
