package news.com.news.network;

import com.google.gson.JsonObject;

import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.Single;
import news.com.news.model.Article;
import news.com.news.model.NewsModel;
import news.com.news.model.RatingResponse;
import news.com.news.model.RegisterTopicResponse;
import news.com.news.model.TopicModel;
import news.com.news.model.UserResponse;
import news.com.news.util.KeyUtil;

public class DataManager {

    private final ApiInterface apiService;

    @Inject
    public DataManager(ApiInterface apiService) {
        this.apiService = apiService;
    }


    public Observable<NewsModel> getSourceNews(String country, String category) {
        ApiInterface apiService =
                RetrofitClass.getClient().create(ApiInterface.class);
        return apiService.getSubCategory(country, category, KeyUtil.API_VALUE);
    }

    public Single<NewsModel> getHeadLineNews(String category) {

        return apiService.getHeadLine(KeyUtil.COUNTRY, category, KeyUtil.API_VALUE);
    }
    public Observable<NewsModel> getHeadLineNewss(String country, String category) {

        return apiService.getHeadLines(country, category, KeyUtil.API_VALUE);
    }

    public Single<NewsModel> getEveryThingNews(String search) {
        ApiInterface apiService =
                RetrofitClass.getClient().create(ApiInterface.class);
        return apiService.getEveryThing(search,KeyUtil.LANGUAGE_AR,KeyUtil.API_VALUE);
       // return null;
    }
    public Single<UserResponse> addUser(JsonObject jsonObject) {
        return apiService.addUser("application/json",jsonObject);
    }
    public Single<UserResponse> login(JsonObject jsonObject) {
        return apiService.login("application/json",jsonObject);
    }
    public Single<List<Article>> getNews() {
        return apiService.getNews();
    }
    public Single<List<Article>> getFavoriteNews(String userName) {
        return apiService.getFavoriteNews(userName);
    }
    public Single<List<Article>> getNewsByTopic(int topic) {
        return apiService.getNewsByTopic(topic);
    }
    public Single<List<TopicModel>> getTopics() {
        return apiService.getTopics();
    }
    public Observable<RatingResponse> saveRating(String userName,JsonObject jsonObject) {
        return apiService.saveRating(userName,jsonObject);
    }
    public Single<RegisterTopicResponse> addTopic(Map<String,String> userName, JsonObject jsonObject) {
        return apiService.addTopicToUser(userName,jsonObject);
    }

//    public Observable<NewsModel> getEveryThingNews(String country,String category) {
//        ApiInterface apiService =
//                RetrofitClass.getClient().create(ApiInterface.class);
//        return apiService.getEveryThing(country,category,KeyUtil.API_VALUE);
//        // return null;
//    }
}
