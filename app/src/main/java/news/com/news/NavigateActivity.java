package news.com.news;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import news.com.news.adapter.NewsTypeAdapter;
import news.com.news.adapter.NotificationAdapter;
import news.com.news.interfaces.OnFavoriteClick;
import news.com.news.main_screen.FavoriteFragment;
import news.com.news.main_screen.MainScreen;
import news.com.news.main_screen.MyFavoriteScreen;
import news.com.news.main_screen.ProfileScreen;
import news.com.news.main_screen.SearchScreen;
import news.com.news.model.FavoriteModel;
import news.com.news.util.FavoriteData;

public class NavigateActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,OnFavoriteClick {

    @Override
    protected int layoutRes() {
        return R.layout.activity_navigate;
    }
    @Inject
    ViewModelFactory viewModelFactory;
    private MainScreenViewModel viewModel;
//    @BindView(R.id.recyclerNotification)
    RecyclerView recyclerNotification;
    @BindView(R.id.recyclerTypes)
    RecyclerView recyclerTypes;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        ImageButton menuLeft =  findViewById(R.id.menuLeft);
        ImageButton menuRight =  findViewById(R.id.menuRight);
         recyclerNotification = findViewById(R.id.recyclerNotification);

         viewModel = ViewModelProviders.of(this,viewModelFactory).get(MainScreenViewModel.class);

         menuLeft.setOnClickListener(v -> {
            if (drawer.isDrawerOpen(GravityCompat.START)) {
                drawer.closeDrawer(GravityCompat.START);
            } else {
                drawer.openDrawer(GravityCompat.START);
            }
        });

        menuRight.setOnClickListener(v -> {
            if (drawer.isDrawerOpen(GravityCompat.END)) {
                drawer.closeDrawer(GravityCompat.END);
            } else {
                drawer.openDrawer(GravityCompat.END);
            }
        });
        if(savedInstanceState == null) {
            FavoriteData favoriteData = new FavoriteData();
           ArrayList<FavoriteModel> favoriteModels = favoriteData.getArrayList(this,"myFavorite");
            if(favoriteModels!=null&&favoriteModels.size()>0) {
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainScreen(), "main").addToBackStack("main").commit();
            }else {
                getSupportFragmentManager().beginTransaction().replace(R.id.container, new FavoriteFragment(), "favorite").commit();

            }
        }

        NavigationView navRight =  findViewById(R.id.navRight);
        NavigationView navLeft =  findViewById(R.id.navLeft);
        navRight.setNavigationItemSelectedListener(this);
        navLeft.setNavigationItemSelectedListener(this);
         recyclerNotification.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
         recyclerNotification.setLayoutManager(new LinearLayoutManager(this));
         recyclerTypes.setLayoutManager(new LinearLayoutManager(this));
         recyclerTypes.addItemDecoration(new DividerItemDecoration(this,DividerItemDecoration.VERTICAL));
         FavoriteData favoriteData = new FavoriteData();
        recyclerNotification.setAdapter(new NotificationAdapter(favoriteData.buildArray()));
         recyclerTypes.setAdapter(new NewsTypeAdapter(favoriteData.buildArrays(), this));

         View header = navRight.getHeaderView(0);
         ImageView imgFavorite =  header.findViewById(R.id.imgFavorite);
         ImageView imgProfile =  header.findViewById(R.id.imgProfile);

         ImageView imgSearch =  header.findViewById(R.id.imgSearch);
         imgSearch.setOnClickListener(v -> {
             getSupportFragmentManager().beginTransaction().replace(R.id.container, new SearchScreen(),"search").commit();
             if (drawer.isDrawerOpen(GravityCompat.START)) {
                 drawer.closeDrawer(GravityCompat.START);
             } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                 drawer.closeDrawer(GravityCompat.END);
             }

         });

         imgProfile.setOnClickListener(v -> {
             getSupportFragmentManager().beginTransaction().replace(R.id.container, new ProfileScreen(),"profile").commit();
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                 drawer.closeDrawer(GravityCompat.START);
             } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                 drawer.closeDrawer(GravityCompat.END);
             }
         });

         imgFavorite.setOnClickListener(v -> {
             getSupportFragmentManager().beginTransaction().replace(R.id.container, new MyFavoriteScreen(),"favorite").commit();
             if (drawer.isDrawerOpen(GravityCompat.START)) {
                 drawer.closeDrawer(GravityCompat.START);
             } else if (drawer.isDrawerOpen(GravityCompat.END)) {
                 drawer.closeDrawer(GravityCompat.END);
             }
         });
     }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer =  findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.navigate, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        MainScreen mainScreen = (MainScreen) getSupportFragmentManager().findFragmentByTag("main");

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }else if (id == R.id.nav_all) {
           // viewModel.getData();
            if (mainScreen != null) {
                mainScreen.getAllData();
            }

        }else if (id == R.id.nav_business) {
            if (mainScreen != null) {
                mainScreen.getNewsByTopic(6);
            }

        }else if (id == R.id.nav_health) {
            if (mainScreen != null) {
                mainScreen.getNewsByTopic(4);
            }
        }else if (id == R.id.nav_science) {
            if (mainScreen != null) {
                mainScreen.getNewsByTopic(3);
            }
        }else if (id == R.id.nav_technology) {
            if (mainScreen != null) {
                mainScreen.getNewsByTopic(2);
            }
        } else if (id == R.id.nav_sport) {
            if (mainScreen != null) {
                mainScreen.getNewsByTopic(2);
            }
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawer(GravityCompat.END);
        return true;
    }

    @Override
    public void onAllClick(FavoriteModel favoriteModel, int pos) {
        MainScreen mainScreen = (MainScreen) getSupportFragmentManager().findFragmentByTag("main");

        if(favoriteModel.getFavoriteServerName().equalsIgnoreCase("All")){
            getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainScreen(), "main").commit();

        }else {
            if (mainScreen != null) {
                Fragment fragment = new MainScreen();
                Bundle bundle = new Bundle();
                bundle.putString("favorite",favoriteModel.getFavoriteServerName());
                bundle.putString("favoriteName",favoriteModel.getFavoriteName());
                fragment.setArguments(bundle);
                getSupportFragmentManager().beginTransaction().replace(R.id.container,fragment, "main").commit();
            }
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        drawer.closeDrawer(GravityCompat.END);
    }
}
