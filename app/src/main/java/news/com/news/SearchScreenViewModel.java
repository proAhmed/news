package news.com.news;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import news.com.news.local.NewsDb;
import news.com.news.main_screen.NewsDetails;
import news.com.news.model.Article;
import news.com.news.model.FavoriteModel;
import news.com.news.model.NewsModel;
import news.com.news.network.DataManager;
import news.com.news.util.FavoriteData;
import news.com.news.util.KeyUtil;

public class SearchScreenViewModel extends ViewModel {

    private CompositeDisposable disposable;
    private final DataManager dataManager;
    private final MutableLiveData<NewsModel> newsModelMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();

    @Inject
    Context context;
    @Inject
    NewsDb newsDb;
    @Inject
    public SearchScreenViewModel(DataManager dataManager) {
        this.dataManager = dataManager;
        disposable = new CompositeDisposable();
        // getData();
    }

    public LiveData<NewsModel> getRepos() {
        return newsModelMutableLiveData;
    }

    public LiveData<Boolean> getError() {
        return repoLoadError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }


    public void getSearchData(String search){
                disposable.add(dataManager.getEveryThingNews(search).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<NewsModel>() {
                    @Override
                    public void onSuccess(NewsModel value) {
                        repoLoadError.setValue(false);
                        newsModelMutableLiveData.setValue(value);
                        loading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                    }
                }));
    }


    public void moveToAnotherActivity(Article article) {
        Intent intent = new Intent(context, NewsDetails.class);
        intent.putExtra(KeyUtil.ARTICLE, article);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }
    public void insertArticle(Article article){
        newsDb.articleDao().insert(article);

    }
    public void removeArticle(Article article){
        newsDb.articleDao().delete(article);

    }
    public List<Article> getFavoriteArticles(){
        Single<List<Article>> articles = newsDb.articleDao().getArticleList();
        return articles.blockingGet();
    }
    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
