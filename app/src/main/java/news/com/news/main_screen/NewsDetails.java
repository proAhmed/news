package news.com.news.main_screen;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import butterknife.BindView;
import news.com.news.BaseActivity;
import news.com.news.R;
import news.com.news.model.Article;
import news.com.news.util.DateUtil;
import news.com.news.util.KeyUtil;
import news.com.news.util.TimeCounter;

public class NewsDetails extends BaseActivity {

    @BindView(R.id.imgNews)
    ImageView imgNews;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvDetails)
    TextView tvDetails;
    @BindView(R.id.tvSource)
    TextView tvSource;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.btnOpenDetails)
    Button btnOpenDetails;
    int time,finalValue;
    @Override
    protected int layoutRes() {
        return R.layout.activity_news_details;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        if(intent!=null&&intent.getExtras()!=null&&intent.getExtras().getParcelable(KeyUtil.ARTICLE)!=null){
            Article article = intent.getExtras().getParcelable(KeyUtil.ARTICLE);
            if (article != null) {
                Picasso.get().load(article.getUrlToImage()).into(imgNews);
                tvTitle.setText(article.getTitle());
                tvDetails.setText(article.getDescription());
                tvSource.setText(" المصدر: "+article.getSource().getName());
                String publishedDate = DateUtil.convertDate(article.getPublishedAt());
                tvDate.setText(publishedDate);
               time = TimeCounter.countRate();
                btnOpenDetails.setOnClickListener(v -> {
                    finalValue = TimeCounter.returnRating(1,time);
                    String url = article.getUrl();
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(url));
                    startActivity(i);
                });

            }
        }
     }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finalValue = TimeCounter.returnRating(0,time);

    }
}
