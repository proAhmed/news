package news.com.news.main_screen;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import news.com.news.BaseFragment;
import news.com.news.NavigateActivity;
import news.com.news.R;
import news.com.news.adapter.FavoriteAdapter;
import news.com.news.adapter.TopicsAdapter;
import news.com.news.interfaces.OnFavoriteClick;
import news.com.news.interfaces.OnTopicClick;
import news.com.news.login.LoginActivity;
import news.com.news.model.FavoriteModel;
import news.com.news.model.RegisterTopicResponse;
import news.com.news.model.TopicModel;
import news.com.news.model.UserResponse;
import news.com.news.network.DataManager;
import news.com.news.util.FavoriteData;

public class FavoriteFragment extends BaseFragment implements OnFavoriteClick, OnTopicClick {
    private ArrayList<FavoriteModel> favoriteModelsAdded, favoriteModelsListNoAdded;
    @BindView(R.id.recyclerViewFavorite)
    RecyclerView recyclerViewFavorite;
    @BindView(R.id.btnMyFavorite)
    Button btnMyFavorite;
    @BindView(R.id.btnAddFavorite)
    Button btnAddFavorite;
    @BindView(R.id.btnNextPage)
    Button btnNextPage;
    @Inject
    DataManager dataManager;
    private OnTopicClick onTopicClick;
    //    @Inject
//    NewsRepository repository;
    private FavoriteData favoriteData;
    private int favoriteAdded = 1;
    String fromProfile = "";
    private CompositeDisposable disposable;


    private FavoriteAdapter favoriteAdapter;

    @Override
    protected int layoutRes() {
        return R.layout.favorite_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        favoriteData = new FavoriteData();
        onTopicClick = this;
        TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
        toolbar_title.setText("المفضلة");
        disposable = new CompositeDisposable();
        getBaseActivity().findViewById(R.id.menuLeft).setVisibility(View.GONE);
        getBaseActivity().findViewById(R.id.menuRight).setVisibility(View.GONE);
        //  setData();
        getTopics();
//          favoriteData.sharedBuild(getBaseActivity());
        btnAddFavorite.setOnClickListener(v -> {
            favoriteAdded = 1;
            //  favoriteModels.clear();
//            ArrayList<FavoriteModel> favoriteModels = favoriteData.filteredAddMyFavorite(favoriteModelsAdded);
            recyclerViewFavorite.setAdapter(null);
            favoriteAdapter = new FavoriteAdapter(favoriteModelsAdded, this);
            recyclerViewFavorite.setAdapter(favoriteAdapter);
        });
        btnMyFavorite.setOnClickListener(v -> {
            favoriteAdded = 0;
            // favoriteModels.clear();
//           ArrayList<FavoriteModel> favoriteModels = favoriteData.filteredMyFavorite(favoriteModelsListNoAdded);
            recyclerViewFavorite.setAdapter(null);
            favoriteAdapter = new FavoriteAdapter(favoriteModelsListNoAdded, this);
            recyclerViewFavorite.setAdapter(favoriteAdapter);

        });
        btnNextPage.setOnClickListener(v -> {
            favoriteData.saveArrayList(getBaseActivity(), favoriteModelsAdded, "favorite");
            favoriteData.saveArrayList(getBaseActivity(), favoriteModelsListNoAdded, "myFavorite");
            getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainScreen(), "main").commit();
        });
    }

    private void setData() {
//        repository.insertAll(favoriteModels);
//        ArrayList<Object> favoriteModels1=   repository.findAll().collect(ArrayList::new, ArrayList::add)
//                .blockingGet();

        favoriteModelsListNoAdded = new ArrayList<>();
        favoriteData.getArrayList(getBaseActivity(), "favorite");
        if (favoriteData.getArrayList(getBaseActivity(), "myFavorite") != null &&
                favoriteData.getArrayList(getBaseActivity(), "myFavorite").size() > 0) {
            favoriteModelsListNoAdded = favoriteData.getArrayList(getBaseActivity(), "myFavorite");
        }
        if (favoriteModelsListNoAdded != null && favoriteModelsListNoAdded.size() > 0) {
            favoriteModelsAdded = favoriteData.getArrayList(getBaseActivity(), "favorite");
        } else {
            favoriteModelsAdded = favoriteData.buildArray();
        }
        Bundle bundle = getArguments();
        if (bundle != null) {
            fromProfile = bundle.getString("title", "");
        }
        if (fromProfile.isEmpty() && favoriteModelsListNoAdded != null && favoriteModelsListNoAdded.size() > 0) {
            getBaseActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, new MainScreen()).commit();
        }
        recyclerViewFavorite.setLayoutManager(new LinearLayoutManager(getActivity()));
        favoriteAdapter = new FavoriteAdapter(favoriteModelsAdded, this);
        recyclerViewFavorite.setAdapter(favoriteAdapter);
    }

    @Override
    public void onAllClick(FavoriteModel favoriteModel, int position) {
        favoriteData.setKey(favoriteModel.getFavoriteServerName(), favoriteModel.getFavoriteServerName());
        favoriteAdapter.notifyDataSetChanged();
        favoriteAdapter.notifyItemRemoved(position);
        if (favoriteAdded == 1) {
            if (favoriteModelsListNoAdded != null && !favoriteModelsListNoAdded.contains(favoriteModel)) {
                favoriteModelsListNoAdded.add(favoriteModel);
            }
            favoriteModelsAdded.remove(favoriteModel);
        } else {
            if (favoriteModelsAdded != null && !favoriteModelsAdded.contains(favoriteModel)) {
                favoriteModelsAdded.add(favoriteModel);
            }
            favoriteModelsListNoAdded.remove(favoriteModel);

        }


    }

    private void getTopics() {
        Single<List<TopicModel>> observable = dataManager.getTopics();
        disposable.add(observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<List<TopicModel>>() {
                    @Override
                    public void onSuccess(List<TopicModel> value) {
                        if (value != null) {
                            recyclerViewFavorite.setAdapter(new TopicsAdapter((ArrayList<TopicModel>) value, onTopicClick));
                        }


                        //    articleDao.insertAll(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("pppp", e.toString());

                    }
                }));
    }

    private void addTopic(String topicName){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name",topicName);
        Map<String,String> hash = new HashMap<>();
        hash.put("Content-Type","application/json");
        hash.put("username",favoriteData.getUserName(getBaseActivity()));
        Single<RegisterTopicResponse> observable =  dataManager.addTopic(hash,jsonObject);
        disposable.add(observable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<RegisterTopicResponse>() {
                    @Override
                    public void onSuccess(RegisterTopicResponse value) {
                        if (value != null) {
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("pppp", e.toString());

                    }
                }));
    }
    @Override
    public void onAllClick(TopicModel topicModel, int pos) {
        addTopic(topicModel.getName());

    }

}
