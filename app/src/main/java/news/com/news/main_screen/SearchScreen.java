package news.com.news.main_screen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import news.com.news.BaseFragment;
import news.com.news.MainScreenViewModel;
import news.com.news.R;
import news.com.news.SearchScreenViewModel;
import news.com.news.ViewModelFactory;
import news.com.news.adapter.NewsMainAdapter;
import news.com.news.adapter.SearchAdapter;
import news.com.news.interfaces.OnAllClickPos;
import news.com.news.model.Article;

public class SearchScreen extends BaseFragment implements OnAllClickPos {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView errorTextView;
    @BindView(R.id.loading_view)
    View loadingView;
    @BindView(R.id.edSearch)
    EditText edSearch;
    @BindView(R.id.imgSearch)
    ImageView imgSearch;
    @Inject
    ViewModelFactory viewModelFactory;
    private SearchScreenViewModel viewModel;
    @Override
    protected int layoutRes() {
        return R.layout.search_fragment;
    }
     @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
         TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
        toolbar_title.setText("الأخبار");
        viewModel = ViewModelProviders.of(this,viewModelFactory).get(SearchScreenViewModel.class);
         imgSearch.setOnClickListener(v -> {
            getAllData(edSearch.getText().toString());

        });
    }
    private void getAllData(String search){

        viewModel.getSearchData(search);
//        recyclerView = view.findViewById(R.identityHashCode.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(),DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(new SearchAdapter(this,viewModel,this));
        observableViewModel();
    }
    private void observableViewModel(){
        viewModel.getRepos().observe(this,newModel->{
            if (newModel!=null){
                recyclerView.setVisibility(View.VISIBLE);
            }

        });
        viewModel.getError().observe(this, error -> {
            if(error!=null){
                if(error){
                    errorTextView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    errorTextView.setText("error");
                }
            }
        });
        viewModel.getLoading().observe(this, isLoading -> {
            if(isLoading!=null){
                if(isLoading){
                    loadingView.setVisibility(View.VISIBLE);
                    errorTextView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                }else {
                    loadingView.setVisibility(View.GONE);

                }
            }
        });
    }

    @Override
    public void onAllClick(Article article,int adapterPosition) {
        viewModel.moveToAnotherActivity(article);

    }

    @Override
    public void onAddArticleToFavorite(Article article, int adapterPosition) {
        viewModel.insertArticle(article);
    }
}
