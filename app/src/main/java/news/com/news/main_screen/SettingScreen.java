package news.com.news.main_screen;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import news.com.news.BaseFragment;
import news.com.news.R;
import news.com.news.SearchScreenViewModel;
import news.com.news.ViewModelFactory;
import news.com.news.adapter.MyFavoriteAdapter;
import news.com.news.interfaces.OnAllClickPos;
import news.com.news.model.Article;

public class SettingScreen extends BaseFragment   {
    @BindView(R.id.tvDetails)
    TextView tvDetails;

    @Override
    protected int layoutRes() {
        return R.layout.setting_fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
        Bundle bundle = getArguments();
        if (bundle != null) {
          String title =  bundle.getString("title","");
            toolbar_title.setText(title);
            String details =  bundle.getString("details","");
            tvDetails.setText(details);
        }
      }




}
