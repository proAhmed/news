package news.com.news.main_screen;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import news.com.news.BaseFragment;
import news.com.news.MainScreenViewModel;
import news.com.news.R;
import news.com.news.ViewModelFactory;
import news.com.news.adapter.NewsMainAdapter;
import news.com.news.interfaces.OnAllClickPos;
import news.com.news.model.Article;
import news.com.news.util.FavoriteData;

public class MainScreen extends BaseFragment implements OnAllClickPos {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tv_error)
    TextView errorTextView;
    @BindView(R.id.loading_view)
    View loadingView;
    @BindView(R.id.btnFavorite)
    Button btnFavorite;
    @BindView(R.id.btnMyFavorite)
    Button btnMyFavorite;
    @Inject
    ViewModelFactory viewModelFactory;
    private MainScreenViewModel viewModel;
    private FavoriteData favoriteData;
    @Override
    protected int layoutRes() {
        return R.layout.main_pager_fragment;
    }
     @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
         TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
         favoriteData = new FavoriteData();
         String userName = favoriteData.getUserName(getBaseActivity());

         toolbar_title.setText("الأخبار");
         getBaseActivity().findViewById(R.id.menuLeft).setVisibility(View.VISIBLE);
         getBaseActivity().findViewById(R.id.menuRight).setVisibility(View.VISIBLE);
        Bundle bundle = getArguments();
         viewModel = ViewModelProviders.of(this,viewModelFactory).get(MainScreenViewModel.class);
         if(bundle!=null&&!bundle.getString("favorite","").isEmpty()) {
//            String favoriteServer = bundle.getString("favorite","");
//            String favorite = bundle.getString("favoriteName","");
//            getFavoriteData(favoriteServer,favorite);
             viewModel.getFavoriteData(userName);

         }else {
            getAllData();
        }
          btnFavorite.setOnClickListener(v -> {
            getAllData();
        });

        btnMyFavorite.setOnClickListener(v -> {
            viewModel.getFavoriteData(userName);
            recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(),DividerItemDecoration.VERTICAL));
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setAdapter(new NewsMainAdapter(this,viewModel,this));
            observableViewModel();
        });
    }
    public void getAllData(){

        viewModel.getData();
//        recyclerView = view.findViewById(R.identityHashCode.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(),DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(new NewsMainAdapter(this,viewModel,this));
        observableViewModel();
    }
    public void getFavoriteData(String favorite,String favoriteKey){
           viewModel.getData();
//        viewModel.getFavoriteData(favorite,favoriteKey);
//        recyclerView = view.findViewById(R.identityHashCode.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(),DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(new NewsMainAdapter(this,viewModel,this));
        observableViewModel();
    }
    public void getNewsByTopic(int topic){
        viewModel.getChosenNews(topic);
//        viewModel.getFavoriteData(favorite,favoriteKey);
//        recyclerView = view.findViewById(R.identityHashCode.recyclerView);
        recyclerView.addItemDecoration(new DividerItemDecoration(getBaseActivity(),DividerItemDecoration.VERTICAL));
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseActivity()));
        recyclerView.setAdapter(new NewsMainAdapter(this,viewModel,this));
        observableViewModel();
    }
    private void observableViewModel(){
        viewModel.getRepos().observe(this,newModel->{
            if (newModel!=null){
                recyclerView.setVisibility(View.VISIBLE);
            }

        });
        viewModel.getError().observe(this, error -> {
            if(error!=null){
                if(error){
                    errorTextView.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    errorTextView.setText("error");
                }
            }
        });
        viewModel.getLoading().observe(this, isLoading -> {
            if(isLoading!=null){
                if(isLoading){
                    loadingView.setVisibility(View.VISIBLE);
                    errorTextView.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.GONE);
                }else {
                    loadingView.setVisibility(View.GONE);
                }
            }
        });
    }
    @Override
    public void onAllClick(Article article,int adapterPosition) {
        viewModel.moveToAnotherActivity(article);
    }

    @Override
    public void onAddArticleToFavorite(Article article, int adapterPosition) {
        viewModel.insertArticle(article);
    }
}
