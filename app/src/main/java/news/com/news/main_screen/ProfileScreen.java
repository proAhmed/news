package news.com.news.main_screen;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import javax.inject.Inject;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import news.com.news.BaseFragment;
import news.com.news.R;
import news.com.news.SearchScreenViewModel;
import news.com.news.ViewModelFactory;
import news.com.news.adapter.MyFavoriteAdapter;
import news.com.news.interfaces.OnAllClickPos;
import news.com.news.login.LoginActivity;
import news.com.news.model.Article;
import news.com.news.util.FavoriteData;

public class ProfileScreen extends BaseFragment  {

    @BindView(R.id.tvName)
    TextView tvName;
    @BindView(R.id.edEmail)
    EditText edEmail;
    private FavoriteData favoriteData;
    @BindView(R.id.reModifyFavorite)
    RelativeLayout reModifyFavorite;
    @BindView(R.id.reAboutApp)
    RelativeLayout reAboutApp;
    @BindView(R.id.rePrivacy)
    RelativeLayout rePrivacy;
    @BindView(R.id.reLogout)
    RelativeLayout reLogout;

    @Override
    protected int layoutRes() {
        return R.layout.profile_page;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
         TextView toolbar_title = getBaseActivity().findViewById(R.id.toolbar_title);
        toolbar_title.setText("الرئيسية");
        favoriteData = new FavoriteData();
        tvName.setText(favoriteData.getName(getBaseActivity()));
        edEmail.setText(favoriteData.getEmail(getBaseActivity()));
        action();
    }

    private void action(){
        reModifyFavorite.setOnClickListener(v -> {
            Fragment fragment = new FavoriteFragment();
            Bundle bundle = new Bundle();
            bundle.putString("title","favorite");
            fragment.setArguments(bundle);
            moveFragment(fragment);
        });

        reAboutApp.setOnClickListener(v -> {

            Fragment fragment = new SettingScreen();
            Bundle bundle = new Bundle();
            bundle.putString("title","عن التطبيق");
            bundle.putString("details",getResources().getString(R.string.about));
            fragment.setArguments(bundle);
            moveFragment(fragment);

        });
        rePrivacy.setOnClickListener(v -> {
            Fragment fragment = new SettingScreen();
            Bundle bundle = new Bundle();
            bundle.putString("title","الخصوصية");
            bundle.putString("details",getResources().getString(R.string.privacy_body));
            fragment.setArguments(bundle);
            moveFragment(fragment);
        });
        reLogout.setOnClickListener(v -> {
            favoriteData = new FavoriteData();
            favoriteData.saveName(getBaseActivity(),"");
             favoriteData.saveEmail(getBaseActivity(),"");
            Intent intent = new Intent(getBaseActivity(),LoginActivity.class);
            startActivity(intent);
            getBaseActivity().finish();

        });
    }


    private void moveFragment(Fragment fragment){
        getBaseActivity().getSupportFragmentManager().beginTransaction().add(R.id.container,fragment).addToBackStack("").commitAllowingStateLoss();
    }
}
