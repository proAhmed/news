package news.com.news;


import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainScreenViewModel.class)
    abstract ViewModel bindMainScreenViewModel(MainScreenViewModel mainScreenViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(SearchScreenViewModel.class)
    abstract ViewModel bindSearchScreenViewModel(SearchScreenViewModel searchScreenViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
