package news.com.news.model;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable{

    private String username;
    private String name;
    private String email;
    private String age;
    private String education;
    private boolean workingStatus;
    private String taxes;
    private String gender;
    private String workType;
    private String employee;
    private String password;
    private String phoneNumber;
    private String positionTitle;
    private String city;
    private String occupation; //
    private String area;
    private String dependency;
    private String section; ///workType

//    "username":"GamalOri3",
//            "email":"gamalorabai@gmail.com",
//            "name":"GamalOrabi",
//            "age" :"26",
//            "city":"Zakazek",
//            "area":"Sharkeya",
//            "gender":"male",
//            "phoneNumber":"+2022548865215",
//            "positionTitle":"Software Developer",
//            "education":" BCs",
//            "workingStatus":"true",
//            "accupation":"private",
//            "section":"private",
//            "taxes":"20.23",
//            "dependency":"No",
//            "password":"gamal#1010"


    public User() {
    }

    protected User(Parcel in) {
        username = in.readString();
        name = in.readString();
        email = in.readString();
        age = in.readString();
        education = in.readString();
        workingStatus = in.readByte() != 0;
        taxes = in.readString();
        gender = in.readString();
        workType = in.readString();
        employee = in.readString();
        password = in.readString();
        phoneNumber = in.readString();
        positionTitle = in.readString();
        city = in.readString();
        occupation = in.readString();
        area = in.readString();
        dependency = in.readString();
        section = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(name);
        dest.writeString(email);
        dest.writeString(age);
        dest.writeString(education);
        dest.writeByte((byte) (workingStatus ? 1 : 0));
        dest.writeString(taxes);
        dest.writeString(gender);
        dest.writeString(workType);
        dest.writeString(employee);
        dest.writeString(password);
        dest.writeString(phoneNumber);
        dest.writeString(positionTitle);
        dest.writeString(city);
        dest.writeString(occupation);
        dest.writeString(area);
        dest.writeString(dependency);
        dest.writeString(section);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public boolean isWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(boolean workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getTaxes() {
        return taxes;
    }

    public void setTaxes(String taxes) {
        this.taxes = taxes;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPositionTitle() {
        return positionTitle;
    }

    public void setPositionTitle(String positionTitle) {
        this.positionTitle = positionTitle;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getDependency() {
        return dependency;
    }

    public void setDependency(String dependency) {
        this.dependency = dependency;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }
}
