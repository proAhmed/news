package news.com.news.model;

import com.google.gson.annotations.Expose;

import java.util.Objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "favorite")
public class FavoriteModel {

    @PrimaryKey
    private int id;
    @Expose
    @ColumnInfo(name = "favoriteName")
    private String favoriteName;
    @Expose
    @ColumnInfo(name = "favoriteServerName")
    private String favoriteServerName;
    @Expose
    @ColumnInfo(name = "favoriteId")
    private int favoriteId;
    @Expose
    @ColumnInfo(name = "favoriteImage")
    private int favoriteImage;
    @Expose
    @ColumnInfo(name = "addToUser")
    private int addToUser;

   @Ignore
   public FavoriteModel() {
    }

    public FavoriteModel(String favoriteName, String favoriteServerName, int favoriteId, int favoriteImage) {
        this.favoriteName = favoriteName;
        this.favoriteServerName = favoriteServerName;
        this.favoriteId = favoriteId;
        this.favoriteImage = favoriteImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFavoriteName() {
        return favoriteName;
    }

    public void setFavoriteName(String favoriteName) {
        this.favoriteName = favoriteName;
    }

    public String getFavoriteServerName() {
        return favoriteServerName;
    }

    public void setFavoriteServerName(String favoriteServerName) {
        this.favoriteServerName = favoriteServerName;
    }

    public int getFavoriteId() {
        return favoriteId;
    }

    public void setFavoriteId(int favoriteId) {
        this.favoriteId = favoriteId;
    }

    public int getFavoriteImage() {
        return favoriteImage;
    }

    public void setFavoriteImage(int favoriteImage) {
        this.favoriteImage = favoriteImage;
    }

    public int getAddToUser() {
        return addToUser;
    }

    public void setAddToUser(int addToUser) {
        this.addToUser = addToUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FavoriteModel that = (FavoriteModel) o;
        return Objects.equals(favoriteServerName, that.favoriteServerName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(favoriteServerName);
    }
}
