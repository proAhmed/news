package news.com.news.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RatingResponse {
    @SerializedName("userLogId")
    @Expose
    private Integer userLogId;
    @SerializedName("feedId")
    @Expose
    private Integer feedId;
    @SerializedName("topicId")
    @Expose
    private Integer topicId;
    @SerializedName("readLater")
    @Expose
    private Boolean readLater;
    @SerializedName("notifiedFeed")
    @Expose
    private Boolean notifiedFeed;
    @SerializedName("openedArticle")
    @Expose
    private Boolean openedArticle;
    @SerializedName("completedReading")
    @Expose
    private Boolean completedReading;
    @SerializedName("moreThanThirtyS")
    @Expose
    private Boolean moreThanThirtyS;
    @SerializedName("searchedKeyWord")
    @Expose
    private Boolean searchedKeyWord;
    @SerializedName("keyWord")
    @Expose
    private String keyWord;
    @SerializedName("readingTime")
    @Expose
    private Integer readingTime;

    public Integer getUserLogId() {
        return userLogId;
    }

    public void setUserLogId(Integer userLogId) {
        this.userLogId = userLogId;
    }

    public Integer getFeedId() {
        return feedId;
    }

    public void setFeedId(Integer feedId) {
        this.feedId = feedId;
    }

    public Integer getTopicId() {
        return topicId;
    }

    public void setTopicId(Integer topicId) {
        this.topicId = topicId;
    }

    public Boolean getReadLater() {
        return readLater;
    }

    public void setReadLater(Boolean readLater) {
        this.readLater = readLater;
    }

    public Boolean getNotifiedFeed() {
        return notifiedFeed;
    }

    public void setNotifiedFeed(Boolean notifiedFeed) {
        this.notifiedFeed = notifiedFeed;
    }

    public Boolean getOpenedArticle() {
        return openedArticle;
    }

    public void setOpenedArticle(Boolean openedArticle) {
        this.openedArticle = openedArticle;
    }

    public Boolean getCompletedReading() {
        return completedReading;
    }

    public void setCompletedReading(Boolean completedReading) {
        this.completedReading = completedReading;
    }

    public Boolean getMoreThanThirtyS() {
        return moreThanThirtyS;
    }

    public void setMoreThanThirtyS(Boolean moreThanThirtyS) {
        this.moreThanThirtyS = moreThanThirtyS;
    }

    public Boolean getSearchedKeyWord() {
        return searchedKeyWord;
    }

    public void setSearchedKeyWord(Boolean searchedKeyWord) {
        this.searchedKeyWord = searchedKeyWord;
    }

    public String getKeyWord() {
        return keyWord;
    }

    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord;
    }

    public Integer getReadingTime() {
        return readingTime;
    }

    public void setReadingTime(Integer readingTime) {
        this.readingTime = readingTime;
    }

}
