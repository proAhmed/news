package news.com.news.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserResponse {
    @SerializedName("serial")
    @Expose
    private Integer serial;
    @SerializedName("username")
    @Expose
    private String username;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("age")
    @Expose
    private Integer age;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("positionTitle")
    @Expose
    private String positionTitle;
    @SerializedName("education")
    @Expose
    private String education;
    @SerializedName("workingStatus")
    @Expose
    private Boolean workingStatus;
    @SerializedName("accupation")
    @Expose
    private String accupation;
    @SerializedName("section")
    @Expose
    private String section;
    @SerializedName("taxes")
    @Expose
    private Double taxes;
    @SerializedName("dependency")
    @Expose
    private String dependency;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("nwFeeds")
    @Expose
    private List<Object> nwFeeds = null;
    @SerializedName("topics")
    @Expose
    private List<Object> topics = null;

    public Integer getSerial() {
        return serial;
    }

    public void setSerial(Integer serial) {
        this.serial = serial;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPositionTitle() {
        return positionTitle;
    }

    public void setPositionTitle(String positionTitle) {
        this.positionTitle = positionTitle;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public Boolean getWorkingStatus() {
        return workingStatus;
    }

    public void setWorkingStatus(Boolean workingStatus) {
        this.workingStatus = workingStatus;
    }

    public String getAccupation() {
        return accupation;
    }

    public void setAccupation(String accupation) {
        this.accupation = accupation;
    }

    public String getSection() {
        return section;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public Double getTaxes() {
        return taxes;
    }

    public void setTaxes(Double taxes) {
        this.taxes = taxes;
    }

    public String getDependency() {
        return dependency;
    }

    public void setDependency(String dependency) {
        this.dependency = dependency;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<Object> getNwFeeds() {
        return nwFeeds;
    }

    public void setNwFeeds(List<Object> nwFeeds) {
        this.nwFeeds = nwFeeds;
    }

    public List<Object> getTopics() {
        return topics;
    }

    public void setTopics(List<Object> topics) {
        this.topics = topics;
    }

}
