package news.com.news;

import java.util.ArrayList;

public class DataBuild {

   public static ArrayList<String> buildAgeList(){
        ArrayList<String> ageList =new ArrayList<>();
        for(int i=16;90>i;i++){
            ageList.add(i+"");
        }
        return ageList;
    }
    public static ArrayList<String> buildLearnList(){
        ArrayList<String> ageList =new ArrayList<>();
        ageList.add("أخر مؤهل حصلت عليه");
        ageList.add("دكتوراة");
        ageList.add("ماجستير");
        ageList.add("بكارليوس/ليسانس");
        ageList.add("متوسط");
        ageList.add("اخرى");
        return ageList;
    }
    public static ArrayList<String> buildTaxList(){
        ArrayList<String> ageList =new ArrayList<>();
        ageList.add("أدخل نسبة الفرائض المنقطعة منك");
        ageList.add("10");
        ageList.add("15");
        ageList.add("20");
        ageList.add("25");
        ageList.add("30");
        return ageList;
    }
    public static ArrayList<String> buildGovernateList(){
        ArrayList<String> ageList =new ArrayList<>();
        ageList.add("القاهرة");
        ageList.add("بنى سويف");
        ageList.add("الجيزة");
        ageList.add("المنيا");
        ageList.add("الشرقية");
        ageList.add("القليوبية");
        return ageList;
    }
    public static ArrayList<String> buildCityList(){
        ArrayList<String> ageList =new ArrayList<>();
        ageList.add("القاهرة");
        ageList.add("بنها");
        ageList.add("المنصورة");
        ageList.add("الزقازيق");
        return ageList;
    }
    public static ArrayList<String> buildReligionList(){
        ArrayList<String> ageList =new ArrayList<>();
        ageList.add("اختار الديانة");
        ageList.add("مسلم");
        ageList.add("نصرانى");
        ageList.add("يهودى");
        return ageList;
    }
}
