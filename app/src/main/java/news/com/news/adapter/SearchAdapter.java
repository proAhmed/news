package news.com.news.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;
import news.com.news.MainScreenViewModel;
import news.com.news.R;
import news.com.news.SearchScreenViewModel;
import news.com.news.interfaces.OnAllClickPos;
import news.com.news.model.Article;
import news.com.news.util.DateUtil;


/**
 * Created by ahmed on 1/06/2018.
 */
public class SearchAdapter extends RecyclerView
        .Adapter<SearchAdapter
        .DataObjectHolder> {
    private final List<Article> articleList= new ArrayList<>();

    private OnAllClickPos onClickPos;

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tvTitle, tvDetails, tvDate, tvKeyWord, tvLocation;
        ImageView imgArticle,imgFav,imgOpen;

        DataObjectHolder(View convertView) {
            super(convertView);
            tvTitle = convertView.findViewById(R.id.tvTitle);
            tvDetails = convertView.findViewById(R.id.tvDetails);
            tvDate = convertView.findViewById(R.id.tvDate);
            imgArticle = convertView.findViewById(R.id.imgArticle);
            tvKeyWord = convertView.findViewById(R.id.tvKeyWord);
            imgFav = convertView.findViewById(R.id.imgFav);
            imgOpen = convertView.findViewById(R.id.imgOpen);

        }
    }

    public SearchAdapter(LifecycleOwner lifecycleOwner, SearchScreenViewModel viewModel, OnAllClickPos onClickPos) {
        this.onClickPos = onClickPos;
        viewModel.getRepos().observe(lifecycleOwner,newsModel -> {
            articleList.clear();

            if(newsModel!=null&&newsModel.getArticles()!=null){
                articleList.addAll(newsModel.getArticles());
                notifyDataSetChanged();
            }
        });

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_item, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataObjectHolder holder, final int position) {

        Article article = articleList.get(position);
     //   Log.d("ppppooo", article.getUrlToImage());
        holder.tvTitle.setText(article.getTitle());
        holder.tvDetails.setText(article.getDescription());
        String publishedDate = DateUtil.convertDate(article.getPublishedAt());
        holder.tvDate.setText(publishedDate);
        String keyWord = article.getKeyWord()+" , "+article.getSource().getName();
        holder.tvKeyWord.setText(keyWord);

        holder.itemView.setOnClickListener(v ->
                onClickPos.onAllClick(article,holder.getAdapterPosition()));

        holder.imgOpen.setOnClickListener(v ->{
            onClickPos.onAllClick(article,holder.getAdapterPosition());
        });

        holder.imgFav.setOnClickListener(v ->{
            holder.imgFav.setColorFilter(v.getContext().getResources().getColor(R.color.colorAccent));
            holder.imgFav.setImageResource(R.drawable.favorite_chosen);
            onClickPos.onAddArticleToFavorite(article,holder.getAdapterPosition());
        });

        if(article.getUrlToImage()!=null&&!article.getUrlToImage().isEmpty())
        Picasso.get().load(article.getUrlToImage()).into(holder.imgArticle);

    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }


}
