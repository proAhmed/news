package news.com.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import news.com.news.R;
import news.com.news.interfaces.OnFavoriteClick;
import news.com.news.model.FavoriteModel;


/**
 * Created by ahmed on 1/06/2018.
 */
public class ProfileAdapter extends RecyclerView
        .Adapter<ProfileAdapter
        .DataObjectHolder> {
    private List<FavoriteModel> favoriteList;

    private OnFavoriteClick onClickPos;

    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tvTitle ;
        ImageView imgFavorite,imgAdd;

        DataObjectHolder(View convertView) {
            super(convertView);
            tvTitle = convertView.findViewById(R.id.tvTitle);
            imgFavorite = convertView.findViewById(R.id.imgFavorite);
            imgAdd = convertView.findViewById(R.id.imgAdd);
        }
    }

    public ProfileAdapter(ArrayList<FavoriteModel> favoriteList , OnFavoriteClick onClickPos) {
        this.onClickPos = onClickPos;
        this.favoriteList = favoriteList;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.favorite_item, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataObjectHolder holder, final int position) {

        FavoriteModel favoriteModel = favoriteList.get(position);
     //   Log.d("ppppooo", article.getUrlToImage());
        holder.tvTitle.setText(favoriteModel.getFavoriteName());
        holder.imgFavorite.setImageResource(favoriteModel.getFavoriteImage());
        holder.imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPos.onAllClick(favoriteModel,position);
            }
        });
//        holder.tvAvailable.setText(article.getStatus());
//        holder.tvPrice.setText(article.getPrice());
//        holder.tvLocation.setText(adsAllItems.getAddress());

        final Context context = holder.itemView.getContext();


    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }


}
