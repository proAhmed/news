package news.com.news.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import news.com.news.R;
import news.com.news.interfaces.OnFavoriteClick;
import news.com.news.model.FavoriteModel;


/**
 * Created by ahmed on 1/06/2018.
 */
public class NewsTypeAdapter extends RecyclerView
        .Adapter<NewsTypeAdapter
        .DataObjectHolder> {
    private List<FavoriteModel> favoriteList;

   private OnFavoriteClick onClickPos;
    static class DataObjectHolder extends RecyclerView.ViewHolder {
        TextView tvTitle ;
        ImageView imgNews;

        DataObjectHolder(View convertView) {
            super(convertView);
            tvTitle = convertView.findViewById(R.id.tvTitle);
            imgNews = convertView.findViewById(R.id.imgNews);

        }
    }

    public NewsTypeAdapter(ArrayList<FavoriteModel> favoriteList, OnFavoriteClick onClickPos) {
        this.favoriteList = favoriteList;
        this.onClickPos = onClickPos;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.navigation_type_item, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataObjectHolder holder, final int position) {

        FavoriteModel favoriteModel = favoriteList.get(position);
        holder.tvTitle.setText(favoriteModel.getFavoriteName());
        holder.imgNews.setImageResource(favoriteModel.getFavoriteImage());
        holder.itemView.setOnClickListener(v -> onClickPos.onAllClick(favoriteModel,holder.getAdapterPosition()));


    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }


}
