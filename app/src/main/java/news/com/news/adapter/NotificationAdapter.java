package news.com.news.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import news.com.news.R;
import news.com.news.interfaces.OnFavoriteClick;
import news.com.news.model.FavoriteModel;


/**
 * Created by ahmed on 1/06/2018.
 */
public class NotificationAdapter extends RecyclerView
        .Adapter<NotificationAdapter
        .DataObjectHolder> {
    private List<FavoriteModel> favoriteList;


    static class DataObjectHolder extends RecyclerView.ViewHolder {
        Switch swNotification ;

        DataObjectHolder(View convertView) {
            super(convertView);
            swNotification = convertView.findViewById(R.id.swNotification);

        }
    }

    public NotificationAdapter(ArrayList<FavoriteModel> favoriteList) {
        this.favoriteList = favoriteList;

    }

    @NonNull
    @Override
    public DataObjectHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                               int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.notification_items, parent, false);

        return new DataObjectHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DataObjectHolder holder, final int position) {

        FavoriteModel favoriteModel = favoriteList.get(position);
        holder.swNotification.setText(favoriteModel.getFavoriteName());


    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }


}
