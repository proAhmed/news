package news.com.news;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.SingleSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import news.com.news.local.NewsDb;
import news.com.news.main_screen.NewsDetails;
import news.com.news.model.Article;
import news.com.news.model.FavoriteModel;
import news.com.news.model.NewsModel;
import news.com.news.network.DataManager;
import news.com.news.util.FavoriteData;
import news.com.news.util.KeyUtil;

public class MainScreenViewModel extends ViewModel {

    private CompositeDisposable disposable;
    private final DataManager dataManager;
    private final MutableLiveData<List<Article>> newsModelMutableLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> repoLoadError = new MutableLiveData<>();
    private final MutableLiveData<Boolean> loading = new MutableLiveData<>();
    @Inject
    NewsDb newsDb;

    @Inject
    Context context;

    @Inject
    public MainScreenViewModel(DataManager dataManager) {
        this.dataManager = dataManager;
        disposable = new CompositeDisposable();
        // getData();
    }

    public LiveData<List<Article>> getRepos() {
        return newsModelMutableLiveData;
    }

    public LiveData<Boolean> getError() {
        return repoLoadError;
    }

    public LiveData<Boolean> getLoading() {
        return loading;
    }

    public void getData() {
        loading.setValue(true);
        //  progressBar.setVisibility(View.VISIBLE);
        Single<NewsModel> observable1 = dataManager.getHeadLineNews( "business");
        Single<NewsModel> observable2 = dataManager.getHeadLineNews("health");
        Single<NewsModel> observable3 = dataManager.getHeadLineNews( "science");
        Single<NewsModel> observable4 = dataManager.getHeadLineNews("technology");
        Single<NewsModel> observable5 = dataManager.getHeadLineNews("sports");
        Single<List<Article>> observableNews = dataManager.getNews();

//        Single<NewsModel> result =
//                Single.zip(observable1.subscribeOn(Schedulers.io()),
//                        observable2.subscribeOn(Schedulers
//                                .io()), observable3.subscribeOn(Schedulers.io()),
//                        observable4.subscribeOn(Schedulers.io()),
//                        observable5.subscribeOn(Schedulers.io()), (type1, type2, type3, type4, type5) -> {
//
//                            List<Article> list = new ArrayList<>();
//                            for (int i = 0; i < type1.getArticles().size(); i++) {
//                                type1.getArticles().get(i).setKeyWord("سوق المال");
//                            }
//                            for (int i = 0; i < type2.getArticles().size(); i++) {
//                                type2.getArticles().get(i).setKeyWord("صحة");
//                            }
//                            for (int i = 0; i < type3.getArticles().size(); i++) {
//                                type3.getArticles().get(i).setKeyWord("علوم");
//                            }
//                            for (int i = 0; i < type4.getArticles().size(); i++) {
//                                type4.getArticles().get(i).setKeyWord("تقنية");
//                            }
//                            for (int i = 0; i < type5.getArticles().size(); i++) {
//                                type5.getArticles().get(i).setKeyWord("رياضة");
//                            }
//                            list.addAll(type1.getArticles());
//                            list.addAll(type2.getArticles());
//                            list.addAll(type3.getArticles());
//                            list.addAll(type4.getArticles());
//                            list.addAll(type5.getArticles());
//                            Collections.sort(list, Collections.reverseOrder());
//                            Collections.shuffle(list);
//                            NewsModel newsModel = new NewsModel();
//                            newsModel.setArticles(list);
//                            return newsModel;
//                        });
//        disposable.add(result.observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableSingleObserver<NewsModel>() {
//
//
//                    @Override
//                    public void onSuccess(NewsModel newsModel) {
//                        repoLoadError.setValue(false);
//                        newsModelMutableLiveData.setValue(newsModel);
//                        loading.setValue(false);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        repoLoadError.setValue(true);
//                        loading.setValue(false);
//                    }
//
//
//                }));
        disposable.add(observableNews.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Article>>() {


                    @Override
                    public void onSuccess(List<Article> newsModel) {
                        repoLoadError.setValue(false);
                        newsModelMutableLiveData.setValue(newsModel);
                        loading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                    }


                }));
//        disposable.add(dataManager.getHeadLineNews("eg","").subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<NewsModel>() {
//                    @Override
//                    public void onSuccess(NewsModel value) {
//                        repoLoadError.setValue(false);
//                        newsModelMutableLiveData.setValue(value);
//                        loading.setValue(false);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        repoLoadError.setValue(true);
//                        loading.setValue(false);
//                    }
//                }));


    }
public void getFavoriteData(String userName){
        loading.setValue(true);
        Single<List<Article>> observableNews = dataManager.getFavoriteNews(userName);
        disposable.add(observableNews.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Article>>() {
                    @Override
                    public void onSuccess(List<Article> newsModel) {
                        repoLoadError.setValue(false);
                        newsModelMutableLiveData.setValue(newsModel);
                        loading.setValue(false);
                    }
                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                    }
                }));
    }

    public void getChosenNews(int topics){
        loading.setValue(true);
        Single<List<Article>> observableNews = dataManager.getNewsByTopic(topics);
        disposable.add(observableNews.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Article>>() {
                    @Override
                    public void onSuccess(List<Article> newsModel) {
                        repoLoadError.setValue(false);
                        newsModelMutableLiveData.setValue(newsModel);
                        loading.setValue(false);
                    }
                    @Override
                    public void onError(Throwable e) {
                        repoLoadError.setValue(true);
                        loading.setValue(false);
                    }
                }));
    }


//    public void getFavoriteData(String favorite,String favoriteKey) {
//        disposable.add(dataManager.getHeadLineNews( favorite).subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<NewsModel>() {
//                    @Override
//                    public void onSuccess(NewsModel value) {
//                        repoLoadError.setValue(false);
//                        for(int i=0;i<value.getArticles().size();i++){
//                            value.getArticles().get(i).setKeyWord(favoriteKey);
//                        }
//                     List<Article> articles =  value.getArticles();
//                        Collections.sort(articles, Collections.reverseOrder());
//                        value.setArticles(articles);
//                        newsModelMutableLiveData.setValue(value);
//                        loading.setValue(false);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        repoLoadError.setValue(true);
//                        loading.setValue(false);
//                    }
//                }));
//    }

//    public void getMyFavorite() {
//        ArrayList<Article> articles = new ArrayList<>();
//        FavoriteData favoriteData = new FavoriteData();
//        favoriteData.sharedBuild(context);
//        ArrayList<FavoriteModel> arrayList = favoriteData.getArrayList(context, "myFavorite");
//        loading.setValue(true);
//
//
//        List<Single<NewsModel>> observableList = new ArrayList<>();
//        List<String> listFav = new ArrayList<>();
//        for (int i = 0; i < arrayList.size(); i++) {
//            int finalI = i;
//            observableList.add(dataManager.getHeadLineNews( arrayList.get(i).getFavoriteServerName())
//                    .doOnSuccess(newsModel ->{
//                           List<Article> articleList = newsModel.getArticles();
//                           for(int iii =0;iii<articleList.size();iii++ ){
//                               articleList.get(iii).setKeyWord(arrayList.get(finalI).getFavoriteName());
//                           }
//                    } ).subscribeOn(Schedulers.io()));
//            listFav.add(arrayList.get(i).getFavoriteName());
//        }
//        Single<List<String>> singleList = Single.just(listFav);
//        Single<List<Single<NewsModel>> > singleLists = Single.just(observableList);
//
//        Single<NewsModel> result = Single.<NewsModel, NewsModel>zip(observableList, objects -> {
//            for (Object object : objects) {
//                NewsModel newsModel = (NewsModel) object;
//                articles.addAll(newsModel.getArticles());
////                Log.d("ooo", observableList.get(0).toString());
////                for(int i =0;i<observableList.size();i++){
////
////                }
//            }
////            NewsModel[]aa = (NewsModel[]) objects;
//            Collections.sort(articles, Collections.reverseOrder());
//            Collections.shuffle(articles);
//            NewsModel newsModel = new NewsModel();
//            newsModel.setArticles(articles);
//            return newsModel;
//        });
////        Observable<NewsModel> resultsObjectObservable = Observable.fromArray(singleList).map(new Function<Single<List<NewsModel>>, NewsModel>() {
////            @Override
////            public NewsModel apply(Single<List<NewsModel>> listSingle) throws Exception {
////               // listSingle.
////                return null;
////            }
////
////
////        });
////        singleLists.flatMap(list ->
////                Observable.fromIterable(list)
////                        .map(item -> {
////                                    return item.blockingGet();
////                                }
////
////                        )
////                        .toList()
////                        .toObservable() // Required for RxJava 2.x
////        )
//
//        Single<NewsModel> results = Single.zip
//                ( singleLists,singleList, new BiFunction<List<Single<NewsModel>>,List<String>, NewsModel>() {
//
//
//                    @Override
//                    public NewsModel apply(List<Single<NewsModel>> newsModel, List<String> strings) throws Exception {
//                        ArrayList<Article> articlesList = new ArrayList<>();
//                           for(int i=0;newsModel.size()>0;i++){
//                               Single<NewsModel> single = newsModel.get(i);
//                              String favorite = strings.get(i);
//                              List<Article> singleArticle = single.blockingGet().getArticles();
//                              for(int ii=0;ii<singleArticle.size();ii++){
//                                  singleArticle.get(ii).setAuthor(favorite);
//                              }
//                               articlesList.addAll(singleArticle);
//                           }
//                        Collections.sort(articlesList);
//
//                        NewsModel newsModelNew = new NewsModel();
//                        newsModelNew.setArticles(articlesList);
//                         return newsModelNew;
//                    }
//                });
//
//        disposable.add(result.observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableSingleObserver<NewsModel>() {
//                    @Override
//                    public void onSuccess(NewsModel newsModel) {
//                        repoLoadError.setValue(false);
//                        newsModel.setArticles(articles);
//                        newsModelMutableLiveData.setValue(newsModel);
//                        loading.setValue(false);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        repoLoadError.setValue(true);
//                        loading.setValue(false);
//                    }
//                }));
//
//    }
    private Single<String> stringObservable() {
        return Single.just("D");
    }

    private Single<Integer> integerObservable() {
        return Single.just(1);
    }

    private Single<String> resultObservable(){
        return Single.zip(stringObservable(), integerObservable(), new BiFunction<String, Integer, String>() {
            @Override public String apply(String s, Integer integer) throws Exception {
                return s + integer;
            }
        });
    }

//    private Observable<String> resultObservableLambda(){
//        return Observable.zip(stringObservable(), integerObservable(), (s, i) -> {
//            return s + i;
//        });
//    }
    private ArrayList<Article> articles = new ArrayList<>();

    private Observable<NewsModel> getFlatData(String... favorite) {

        return dataManager.getHeadLineNewss("eg", favorite[0])
                .subscribeOn(Schedulers.io())
                .flatMap((Function<NewsModel, ObservableSource<NewsModel>>) response1 -> {
                    articles.addAll(response1.getArticles());
                    return dataManager.getHeadLineNewss("eg", favorite[1]);
                });
    }

//    private void addData(String... favorite) {
//        Disposable disposable = getFlatData(favorite)
//                .flatMap((Function<NewsModel, ObservableSource<NewsModel>>) response2 -> {
//                    articles.addAll(response2.getArticles());
//                    return dataManager.getHeadLineNewss("eg", favorite[2]);
//                })
//                .map(response3 -> {
//                    articles.addAll(response3.getArticles());
//                    return response3;
//                })
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(new DisposableObserver<NewsModel>() {
//
//                    @Override
//                    public void onNext(NewsModel response3) {
//                        repoLoadError.setValue(false);
//                        articles.addAll(response3.getArticles());
//                        NewsModel newsModel = new NewsModel();
//                        newsModel.setArticles(articles);
//                        newsModelMutableLiveData.setValue(newsModel);
//                        loading.setValue(false);
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        repoLoadError.setValue(true);
//                        loading.setValue(false);
//                    }
//
//                    @Override
//                    public void onComplete() {
//
//                    }
//                });
//    }

    List<Single> arrayLists = new ArrayList<>();

//    public void getMyData() {
//        FavoriteData favoriteData = new FavoriteData();
//        favoriteData.sharedBuild(context);
//        ArrayList<FavoriteModel> arrayList = favoriteData.getArrayList(context, "myFavorite");
//        loading.setValue(true);
//        //  progressBar.setVisibility(View.VISIBLE);
//        if (arrayList != null && arrayList.size() > 0) {
//            String[] array = new String[arrayList.size()];
//            for (int i = 0; i < arrayList.size(); i++) {
//                array[i] = arrayList.get(i).getFavoriteServerName();
//                arrayLists.add(dataManager.getHeadLineNews(arrayList.get(i).getFavoriteServerName()));
//            }
//            addData(array);
////            Single<NewsModel> result =
////                    Single.zip(observable1.subscribeOn(Schedulers.io()), observable2.subscribeOn(Schedulers
////                                    .io()), observable3.subscribeOn(Schedulers.io()),
////                            observable4.subscribeOn(Schedulers.io()),
////                            observable5.subscribeOn(Schedulers.io()), (type1, type2, type3, type4, type5) -> {
////
////                                List<Article> list = new ArrayList<>();
////                                list.addAll(type1.getArticles());
////                                list.addAll(type2.getArticles());
////                                list.addAll(type3.getArticles());
////                                list.addAll(type4.getArticles());
////                                list.addAll(type5.getArticles());
////                                NewsModel newsModel = new NewsModel();
////                                newsModel.setArticles(list);
////                                return newsModel;
////                            });
////            disposable.add(dataManager.getHeadLineNews("eg", favorite).subscribeOn(Schedulers.newThread())
////                    .observeOn(AndroidSchedulers.mainThread()).subscribeWith(new DisposableSingleObserver<NewsModel>() {
////                        @Override
////                        public void onSuccess(NewsModel value) {
////                            repoLoadError.setValue(false);
////                            newsModelMutableLiveData.setValue(value);
////                            loading.setValue(false);
////                        }
////
////                        @Override
////                        public void onError(Throwable e) {
////                            repoLoadError.setValue(true);
////                            loading.setValue(false);
////                        }
////                    }));
//        }
//    }

    public void insertArticle(Article article){
        newsDb.articleDao().insert(article);

    }

    public void moveToAnotherActivity(Article article) {
        Intent intent = new Intent(context, NewsDetails.class);
        intent.putExtra(KeyUtil.ARTICLE, article);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (disposable != null) {
            disposable.clear();
            disposable = null;
        }
    }
}
