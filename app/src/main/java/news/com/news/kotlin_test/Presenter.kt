package news.com.news.kotlin_test

import androidx.lifecycle.MutableLiveData

class Presenter {
    val titleData = MutableLiveData<String>()
    fun showTitle(title:String){
        titleData.postValue(title)
    }


}